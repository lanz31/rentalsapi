﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class AddTransactionHistoryModel
    {
        public DateTime DatePaid { get; set; }
        public decimal AmountDue { get; set; }
        public decimal AmountPaid { get; set; }
        public string PaymentFor { get; set; }
        public decimal Balance { get; set; }
        public Guid PropertyId { get; set; }
        public Guid RenterId { get; set; }
    }
}

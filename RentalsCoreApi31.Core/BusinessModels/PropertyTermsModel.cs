﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class PropertyTermsModel
    {
        [Required]
        public decimal MonthAdvance { get; set; }

        [Required]
        public decimal MonthDeposit { get; set; }

        [Required]
        public Guid RoomId { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class TokenModel
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
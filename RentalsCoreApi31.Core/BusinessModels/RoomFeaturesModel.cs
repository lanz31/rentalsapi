﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class RoomFeaturesModel
    {
        public string Name { get; set; }
        public Guid RoomId { get; set; }
    }
}
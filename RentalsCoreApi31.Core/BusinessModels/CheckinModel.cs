﻿namespace RentalsCoreApi31.Core.BusinessModels
{
    public class CheckinModel
    {
        public long PropertyId { get; set; }
        public long RoomId { get; set; }
        public long RenterId { get; set; }
    }
}
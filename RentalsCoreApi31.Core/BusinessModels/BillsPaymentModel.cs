﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class BillsPaymentModel
    {
        public Guid RenterId { get; set; }
        public DateTime NextDateDue { get; set; }
        public decimal Balance { get; set; }
        public decimal AmountDue { get; set; }
    }
}

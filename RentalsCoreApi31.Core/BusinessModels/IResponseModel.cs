﻿using System.Net;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public interface IResponseModel<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }
    }

    public class Response
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }

    public class ResponseModel<T> : IResponseModel<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }
    }
}

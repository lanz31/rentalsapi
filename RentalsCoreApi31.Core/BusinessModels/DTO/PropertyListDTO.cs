﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class PropertyListDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string City { get; set; }
        public int TotalRooms { get; set; }
    }
}

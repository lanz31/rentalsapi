﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class RenterInfoDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public string Profession { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public long PropertyId { get; set; }
        public string Email { get; set; }
        public string DateCreated { get ; set; }
    }
}

﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class AvailableBedsDTO
    {
        public long RoomId { get; set; }
        public int BedsOccupied { get; set; }
        public int BedsAvailable { get; set; }
    }
}
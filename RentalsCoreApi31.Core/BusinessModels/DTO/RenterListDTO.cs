﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class RenterListDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime? CheckIn { get; set; }
        public string ContactNo { get; set; }
        public string Profession { get; set; }
        public long PropertyId { get; set; }
        public long RoomId { get; set; }
    }
}
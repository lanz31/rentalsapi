﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class PropertyInfoDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ContactNo { get; set; }
        public int TotalRooms { get; set; }
    }
}
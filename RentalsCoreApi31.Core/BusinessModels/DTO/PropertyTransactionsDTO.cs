﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class PropertyTransactionsDTO
    {
        public long UserId { get; set; }
        public long PropertyId { get; set; }
        public long TransactionId { get; set; }
        public DateTime DatePaid { get; set; }
        public double AmountDue { get; set; }
        public DateTime NextDateDue { get; set; }
        public DateTime DaysBeforeDue { get; set; }
        public long RenterId { get; set; }
        public double Balance { get; set; }
        public string PaymentFor { get; set; }
        public double AmountPaid { get; set; }
    }
}
﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class RoomsListInfoDTO
    {
        public long RoomId { get; set; }
        public string RoomName { get; set; }
        public int TotalBeds { get; set; }
        public int OccupiedBeds { get; set; }
        public string RoomType { get; set; }
        public decimal Price { get; set; }
        public decimal MonthsAdvance { get; set; }
        public decimal MonthsDeposit { get; set; }
    }
}
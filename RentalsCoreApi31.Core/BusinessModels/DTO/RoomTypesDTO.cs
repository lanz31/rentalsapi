﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class RoomTypesDTO
    {
        public long RoomTypeId { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public decimal MonthsAdvance { get; set; }
        public decimal MonthsDeposit { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class RoomInfoDTO
    {
        public long RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeName { get; set; }
        public int TotalBeds { get; set; }
        public int OccupiedBeds { get; set; }
        public decimal Price { get; set; }
        public decimal MonthsAdvance { get; set; }
        public decimal MonthsDeposit { get; set; }
        public IEnumerable<string> Features { get; set; }
    }
}
﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels.DTO
{
    public class UpdateRenterInfoDTO
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}
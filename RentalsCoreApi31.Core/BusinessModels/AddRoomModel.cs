﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class AddRoomModel
    {
        public string Name { get; set; }
        public int TotalBeds { get; set; }
        public long RoomTypeId { get; set; }
        public long PropertyId { get; set; }
    }
}
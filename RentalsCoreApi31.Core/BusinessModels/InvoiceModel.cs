﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class InvoiceModel
    {
        public decimal AmountDue { get; set; }
        public DateTime BillingDue { get; set; }
        public long RenterId { get; set; }
    }
}

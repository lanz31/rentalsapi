﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class GenerateInvoiceModel
    {
        public decimal RoomPrice { get; set; }
        public decimal MonthsAdv { get; set; }
        public decimal MonthsDep { get; set; }
        public Guid RenterId { get; set; }
        public DateTime CheckInDate { get; set; }
    }
}

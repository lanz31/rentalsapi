﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class AddRenterModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Profession { get; set; }
        public long PropertyId { get; set; }
    }
}
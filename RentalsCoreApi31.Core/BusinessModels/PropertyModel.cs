﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class PropertyModel
    {
        public Guid PropertyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ContactNo { get; set; }
        public int TotalRooms { get; set; }
        public Guid UserId { get; set; }
    }
}
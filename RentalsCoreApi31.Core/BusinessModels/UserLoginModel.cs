﻿namespace RentalsCoreApi31.Core.BusinessModels
{
    public class UserLoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}

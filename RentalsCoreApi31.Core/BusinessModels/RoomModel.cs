﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class RoomModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int TotalBeds { get; set; }
        public Guid RoomTypeId { get; set; }
        public Guid PropertyId { get; set; }
    }
}
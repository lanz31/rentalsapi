﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class RoomFloorPlanModel
    {
        public string Img { get; set; }
        public Guid RoomId { get; set; }
    }
}
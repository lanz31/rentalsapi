﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class AddRoomFeaturesModel
    {
        public string Name { get; set; }
        public long RoomId { get; set; }
    }
}
﻿using System;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class AvailableBedsModel
    {
        public Guid UserId { get; set; }
        public Guid PropertyId { get; set; }
        public Guid RoomId { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RentalsCoreApi31.Core.BusinessModels
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public string Salt { get; set; }
    }
}

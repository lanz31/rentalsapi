﻿using System;
using System.IO;

namespace RentalsCoreApi31.Core.Services
{
    public interface ILoggerService
    {
        void Log(string feature, string innerex, string message, string stacktrace);
    }
}
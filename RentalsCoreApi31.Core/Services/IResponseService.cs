﻿using RentalsCoreApi31.Core.BusinessModels;

using System.Net;

namespace RentalsCoreApi31.Core.Services
{
    public interface IResponseService<T>
    {
        IResponseModel<T> Response(bool status, string message, HttpStatusCode statusCode, T data);
        IResponseModel<T> Response(bool status, string message, HttpStatusCode statusCode);
    }

    public class ResponseService<T> : IResponseService<T>
    {
        public IResponseModel<T> _response { get; set; }

        public ResponseService(IResponseModel<T> response)
        {
            _response = response;
        }

        public IResponseModel<T> Response(bool status, string message, HttpStatusCode statusCode, T data)
        {
            _response.Status = status;
            _response.Message = message;
            _response.StatusCode = statusCode;
            _response.Data = data;

            return _response;
        }

        public IResponseModel<T> Response(bool status, string message, HttpStatusCode statusCode)
        {
            _response.Status = status;
            _response.Message = message;
            _response.StatusCode = statusCode;

            return _response;
        }
    }
}

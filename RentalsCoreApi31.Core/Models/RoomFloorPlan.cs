﻿using System;

namespace RentalsCoreApi31.Core.Models
{
    public class RoomFloorPlan : BaseClass
    {
        public string Img { get; set; }
        public string FilePath { get; set; }
        public string FileExt { get; set; }

        public Room Room { get; set; }
        public Guid RoomId { get; set; }
    }
}
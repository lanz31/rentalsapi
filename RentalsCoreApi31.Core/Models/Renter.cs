﻿using System;
using System.Collections.Generic;

namespace RentalsCoreApi31.Core.Models
{
    public class Renter : BaseClass
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Profession { get; set; }
        public DateTime? CheckIn { get; set; }
        public DateTime? CheckOut { get; set; }
        public long? RoomId { get; set; }
        public bool Status { get; set; }
        public IEnumerable<Invoice> Invoices { get; set; }
        public IEnumerable<TransactionHistory> Transactions { get; set; }

        public RealEstateProperty Property { get; set; }
        public long? PropertyId { get; set; }
    }
}
﻿using System;

namespace RentalsCoreApi31.Core.Models
{
    public class RoomFeatures : BaseClass
    {
        public string Name { get; set; }

        public Room Room { get; set; }
        public long RoomId { get; set; }
    }
}
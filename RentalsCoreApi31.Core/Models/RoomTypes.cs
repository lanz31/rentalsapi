﻿using System.Collections.Generic;

namespace RentalsCoreApi31.Core.Models
{
    public class RoomTypes : BaseClass
    {
        public string Type { get; set; }
        public decimal Price { get; set; }
        public decimal MonthsAdvance { get; set; }
        public decimal MonthsDeposit { get; set; }
        public IEnumerable<Room> Rooms { get; set; }

        public RealEstateProperty Property { get; set; }
        public long PropertyId { get; set; }
    }
}
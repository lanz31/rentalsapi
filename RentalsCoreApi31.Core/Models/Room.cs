﻿using System.Collections.Generic;

namespace RentalsCoreApi31.Core.Models
{
    public class Room : BaseClass
    {
        public string Name { get; set; }
        public int TotalBeds { get; set; }
        public int OccupiedBeds { get; set; }
        public IEnumerable<RoomFeatures> Features { get; set; }

        public RoomTypes RoomType { get; set; }
        public long? RoomTypeId { get; set; }

        public RealEstateProperty Property { get; set; }
        public long PropertyId { get; set; }
    }
}
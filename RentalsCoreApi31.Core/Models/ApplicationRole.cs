﻿using Microsoft.AspNetCore.Identity;

namespace RentalsCoreApi31.Core.Models
{
    public class ApplicationRole : IdentityRole<long>
    {
    }
}

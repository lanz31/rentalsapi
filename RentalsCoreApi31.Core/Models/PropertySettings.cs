﻿using System;

namespace RentalsCoreApi31.Core.Models
{
    public class PropertySettings : BaseClass
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Room Room { get; set; }
        public Guid RoomId { get; set; }
    }
}
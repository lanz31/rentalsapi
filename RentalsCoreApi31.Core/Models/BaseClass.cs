﻿using System;

namespace RentalsCoreApi31.Core.Models
{
    public abstract class BaseClass
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
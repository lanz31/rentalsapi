﻿using System;

namespace RentalsCoreApi31.Core.Models
{
    public class Invoice : BaseClass
    {
        public decimal AmountDue { get; set; }
        public DateTime BillingDue { get; set; }
        public bool Status { get; set; }

        public Renter Renter { get; set; }
        public long? RenterId { get; set; }
    }
}

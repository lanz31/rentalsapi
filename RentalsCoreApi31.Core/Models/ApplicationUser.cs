﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace RentalsCoreApi31.Core.Models
{
    public class ApplicationUser : IdentityUser<long>
    {
        public string Name { get; set; }

        public IEnumerable<RealEstateProperty> Properties { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace RentalsCoreApi31.Core.Models
{
    public class RealEstateProperty : BaseClass
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string City { get; set; }
        public int TotalRooms { get; set; }

        public IEnumerable<Renter> Renters { get; set; }
        public IEnumerable<Room> Rooms { get; set; }
        public IEnumerable<RoomTypes> RoomTypes { get; set; }
        public IEnumerable<TransactionHistory> Transactions { get; set; }

        public ApplicationUser User { get; set; }
        public long UserId { get; set; }
    }
}
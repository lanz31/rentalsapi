﻿using System;

namespace RentalsCoreApi31.Core.Models
{
    public class TransactionHistory : BaseClass
    {
        public decimal AmountDue { get; set; }
        public decimal AmountPaid { get; set; }
        public string PaymentFor { get; set; }
        public decimal Balance { get; set; }

        public RealEstateProperty Property { get; set; }
        public long? PropertyId { get; set; }

        public Renter Renter { get; set; }
        public long? RenterId { get; set; }
    }
}
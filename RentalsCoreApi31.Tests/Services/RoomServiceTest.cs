﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Validator;
using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Room.Queries;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using FluentAssertions;

namespace RentalsCoreApi31.Tests.Services
{
    public class RoomServiceTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private IRoomService _roomSvc;
        private IPropertyService _propertySvc;
        private WebApplicationFactory<Startup> _svcFactory;

        public RoomServiceTest(WebApplicationFactory<Startup> factory)
        {
            _svcFactory = factory;
            var scpFactory = _svcFactory.Services.GetService<IServiceScopeFactory>().CreateScope();

            IConfigurationRoot config = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appsettings.Development.json", optional: false)
               .Build();

            _roomSvc = scpFactory.ServiceProvider.GetService<IRoomService>();
            _propertySvc = scpFactory.ServiceProvider.GetService<IPropertyService>();
        }

        #region Room Type Tests
        [Theory]
        [InlineData(10016, "6 beds fan room", 2750, 2, 1)]
        [InlineData(10016, "6 beds aircon room", 3350, 2, 1)]
        [InlineData(10016, "", 3350, 2, 1)]
        [InlineData(10017, "Queen size bed aircon room", 4000, 2, 1)]
        [InlineData(10017, "Twin bed aircon room", 4000, 2, 1)]
        [InlineData(10017, "", 4000, 2, 1)]
        [InlineData(7, "4 beds aircon room", 3850, 2, 1)]
        [InlineData(0, "", 2, 1, 2)]
        [InlineData(10018, "4 beds aircon room", 3850, 2, 1)]
        [InlineData(10018, "6 bunk beds fan room", 2000, 2, 1)]
        [InlineData(10018, "6 beds aircon room", 3350, 2, 1)]
        [InlineData(10019, "6 beds fan room", 2750, 1, 2)]
        [InlineData(10019, "2 beds aircon room", 3500, 1, 1)]
        public async Task AddRoomTypeAsync_Test(long propid, string type, decimal price, decimal monthAdv, decimal monthDep)
        {
            AddRoomTypeCommandValidator validator = new AddRoomTypeCommandValidator();

            AddRoomTypeCommand addRoomTypeCmd = new AddRoomTypeCommand
            {
                PropertyId = propid,
                Type = type,
                Price = price,
                MonthsAdvance = monthAdv,
                MonthsDeposit = monthDep,
            };

            var result = validator.Validate(addRoomTypeCmd);

            if (result.IsValid)
            {
                IResponseModel<PropertyInfoDTO> property = await _propertySvc.GetPropertyInfoAsync(addRoomTypeCmd.PropertyId);

                if (property.Data == null) property.Should().NotBeNull();

                var isExisting = await _roomSvc.VerifyRoomTypeAsync(property.Data.Id, addRoomTypeCmd.Type);

                if (!isExisting.Status) isExisting.Status.Should().BeFalse();

                AddRoomTypeModel roomType = new AddRoomTypeModel
                {
                    Type = addRoomTypeCmd.Type,
                    Price = addRoomTypeCmd.Price,
                    MonthsAdvance = addRoomTypeCmd.MonthsAdvance,
                    MonthsDeposit = addRoomTypeCmd.MonthsDeposit,
                    PropertyId = addRoomTypeCmd.PropertyId
                };

                var response = await _roomSvc.AddRoomTypesAsync(roomType);
                response.Status.Should().BeTrue();
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
                }

                result.Errors.Should().BeNullOrEmpty();
            }
        }

        [Theory]
        [InlineData(10016)]
        [InlineData(10017)]
        [InlineData(10018)]
        [InlineData(10019)]
        [InlineData(10020)]
        public async Task GetPropertyRoomTypes(long propid)
        {
            GetRoomTypesQueryValidator validator = new GetRoomTypesQueryValidator();

            var getRoomTypes = new GetRoomTypesQuery(propid);

            var result = validator.Validate(getRoomTypes);

            if (result.IsValid)
            {
                IResponseModel<PropertyInfoDTO> property = await _propertySvc.GetPropertyInfoAsync(propid);

                if (property.Data == null) property.Should().NotBeNull();

                var roomTypes = await _roomSvc.GetRoomTypesPerPropertyAsync(property.Data.Id);
                roomTypes.Should().NotBeNull();
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
                }

                result.Errors.Should().BeNullOrEmpty();
            }
        }

        //[Theory]
        //[InlineData(7, 1, "4 beds aircon room", 4850, 2, 1)]
        //[InlineData(10004, 10005, "8 beds fan room", 1950, 2, 1)]
        //[InlineData(10004, 10006, "Single bed aircon room", 2950, 2, 1)]
        //public async Task UpdateRoomTypeAsync_Test(long propid, long roomtypeid, string type, decimal price, decimal monthAdv, decimal monthDep)
        //{
        //    UpdateRoomTypeCommandValidator validator = new UpdateRoomTypeCommandValidator();

        //    UpdateRoomTypeCommand updRoomTypeCmd = new UpdateRoomTypeCommand
        //    {
        //        PropertyId = propid,
        //        RoomTypeId = roomtypeid,
        //        Type = type,
        //        Price = price,
        //        MonthsAdvance = monthAdv,
        //        MonthsDeposit = monthDep
        //    };

        //    var result = validator.Validate(updRoomTypeCmd);

        //    if (result.IsValid)
        //    {
        //        PropertyInfoDTO property = await _propertySvc.GetPropertyInfoAsync(updRoomTypeCmd.PropertyId);
        //        if (property == null) property.Should().NotBeNull();

        //        var roomTypeExist = await _roomSvc.VerifyRoomTypeAsync(property.Id, updRoomTypeCmd.RoomTypeId);
        //        if (!roomTypeExist.Status) roomTypeExist.Status.Should().BeTrue();

        //        UpdateRoomTypeModel roomType = new UpdateRoomTypeModel
        //        {
        //            RoomTypeId = updRoomTypeCmd.RoomTypeId,
        //            Type = updRoomTypeCmd.Type,
        //            Price = updRoomTypeCmd.Price,
        //            MonthsAdvance = updRoomTypeCmd.MonthsAdvance,
        //            MonthsDeposit = updRoomTypeCmd.MonthsDeposit,
        //            PropertyId = updRoomTypeCmd.PropertyId
        //        };

        //        var response = await _roomSvc.UpdateRoomTypeAsync(roomType);
        //        response.Status.Should().BeTrue();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}
        #endregion

        #region Room Tests
        //[Theory]
        //[InlineData("Orchid", 6, 29, 1)]
        //[InlineData("Sunflower", 4, 31, 1)]
        //[InlineData("Sunflower", 6, 20, 1)]
        //[InlineData("Magnolia", 6, 30, 1)]
        //[InlineData("", 0, 0, 0)]
        //[InlineData("Orchid", 6, 33, 2)]
        //[InlineData("Rosal", 6, 33, 2)]
        //[InlineData("Gumamela", 6, 28, 2)]
        //[InlineData("Lilium", 4, 28, 3)]
        //[InlineData("Daisy", 4, 28, 3)]
        //public async Task AddRoomAsync_Test(string name, int beds, long roomtypeid, long propertyid)
        //{
        //    AddRoomCommandValidator validator = new AddRoomCommandValidator();

        //    AddRoomCommand addRoomCmd = new AddRoomCommand
        //    {
        //        Name = name,
        //        TotalBeds = beds,
        //        RoomTypeId = roomtypeid,
        //        PropertyId = propertyid
        //    };

        //    var result = validator.Validate(addRoomCmd);

        //    if (result.IsValid)
        //    {
        //        var isRoomExists = await _roomSvc.VerifyRoomAsync(addRoomCmd.PropertyId, addRoomCmd.Name);

        //        if (!isRoomExists.Status)
        //        {
        //            var isRoomTypeExist = await _roomSvc.VerifyRoomTypeAsync(addRoomCmd.PropertyId, addRoomCmd.RoomTypeId);

        //            if (isRoomTypeExist.Status)
        //            {
        //                AddRoomModel room = new AddRoomModel
        //                {
        //                    Name = addRoomCmd.Name,
        //                    TotalBeds = addRoomCmd.TotalBeds,
        //                    RoomTypeId = addRoomCmd.RoomTypeId,
        //                    PropertyId = addRoomCmd.PropertyId
        //                };

        //                var response = await _roomSvc.AddRoomAsync(room);
        //                response.Status.Should().BeTrue();
        //            }
        //            else
        //                isRoomTypeExist.Status.Should().BeTrue();
        //        }
        //        else
        //            isRoomExists.Status.Should().BeFalse();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData(4, false)]
        //[InlineData(4, true)]
        //public async Task GetRoomsAsync_Test(long propid, bool filter)
        //{
        //    GetRoomQueryValidator validator = new GetRoomQueryValidator();

        //    var getRooms = new GetRoomsQuery(propid, filter);

        //    var result = validator.Validate(getRooms);

        //    if (result.IsValid)
        //    {
        //        IEnumerable<RoomsListInfoDTO> rooms = await _roomSvc.GetPropertyRoomsAsync(propid, filter);
        //        rooms.Should().NotBeNull();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData(1, 73)]
        //[InlineData(1, 74)]
        //[InlineData(1, 76)]
        //[InlineData(2, 75)]
        //[InlineData(2, 77)]
        //[InlineData(3, 72)]
        //[InlineData(3, 78)]
        //public async Task GetRoomInfoAsync_Test(long propid, long roomid)
        //{
        //    GetRoomInfoQueryValidator validator = new GetRoomInfoQueryValidator();

        //    var getRoom = new GetRoomInfoQuery(propid, roomid);

        //    var result = validator.Validate(getRoom);

        //    if (result.IsValid)
        //    {
        //        RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(propid, roomid);
        //        roomInfo.Should().NotBeNull();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}
        #endregion

        #region Room Features Tests
        //[Theory]
        ////[InlineData("Bed w/ mattress", 73)]
        ////[InlineData("Locker", 73)]
        ////[InlineData("Aircon", 73)]
        ////[InlineData("Comfort room", 73)]
        ////[InlineData("Ceiling fan", 73)]
        ////[InlineData("Bed w/ mattress", 74)]
        ////[InlineData("Locker", 74)]
        ////[InlineData("Ceiling fan", 74)]
        ////[InlineData("Bed w/ mattress", 76)]
        ////[InlineData("Locker", 76)]
        ////[InlineData("Aircon", 76)]
        ////[InlineData("Comfort room", 76)]
        ////[InlineData("Ceiling fan", 76)]
        ////[InlineData("Bed w/ mattress", 75)]
        ////[InlineData("Locker", 75)]
        ////[InlineData("Aircon", 75)]
        ////[InlineData("Comfort room", 75)]
        ////[InlineData("Ceiling fan", 75)]
        //[InlineData("Common CR", 75)]
        ////[InlineData("Bed w/ mattress", 77)]
        ////[InlineData("Locker", 77)]
        ////[InlineData("Aircon", 77)]
        ////[InlineData("Comfort room", 77)]
        ////[InlineData("Ceiling fan", 77)]
        ////[InlineData("Bed w/ mattress", 72)]
        ////[InlineData("Locker", 72)]
        ////[InlineData("Aircon", 72)]
        ////[InlineData("Comfort room", 72)]
        ////[InlineData("Ceiling fan", 72)]
        ////[InlineData("Bed w/ mattress", 78)]
        ////[InlineData("Locker", 78)]
        ////[InlineData("Aircon", 78)]
        ////[InlineData("Comfort room", 78)]
        ////[InlineData("Ceiling fan", 78)]
        //public async Task AddRoomFeaturesAsync_Test(string name, long roomid)
        //{
        //    AddRoomFeatureCommandValidator validator = new AddRoomFeatureCommandValidator();

        //    AddRoomFeatureCommand addRoomFeatureCmd = new AddRoomFeatureCommand
        //    {
        //        Name = name,
        //        RoomId = roomid
        //    };

        //    var result = validator.Validate(addRoomFeatureCmd);

        //    if (result.IsValid)
        //    {
        //        RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(0, addRoomFeatureCmd.RoomId);
        //        var isExisting = await _roomSvc.VerifyRoomFeatureAsync(addRoomFeatureCmd.RoomId, addRoomFeatureCmd.Name);

        //        if (roomInfo != null)
        //        {
        //            if (!isExisting.Status)
        //            {
        //                AddRoomFeaturesModel roomFeature = new AddRoomFeaturesModel
        //                {
        //                    Name = name,
        //                    RoomId = roomid
        //                };

        //                var response = await _roomSvc.AddRoomFeatureAsync(roomFeature);
        //                response.Status.Should().BeTrue();
        //            }
        //            else
        //                isExisting.Status.Should().BeTrue();
        //        }
        //        else
        //            roomInfo.Should().NotBeNull();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}
        #endregion
    }
}

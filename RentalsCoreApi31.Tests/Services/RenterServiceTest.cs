﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Validator;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Services;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Queries;
using RentalsCoreApi31.Infrastructure.Operations.Transactions.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;

using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using FluentAssertions;

namespace RentalsCoreApi31.Tests.Services
{
    public class RenterServiceTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        //private ITransactionService _transactionSvc;
        //private IPropertyService _propertySvc;
        //private IRoomService _roomSvc;
        //private IRenterService _renterSvc;
        //private WebApplicationFactory<Startup> _svcFactory;

        //public RenterServiceTest(WebApplicationFactory<Startup> factory)
        //{
        //    _svcFactory = factory;
        //    var scpFactory = _svcFactory.Services.GetService<IServiceScopeFactory>().CreateScope();

        //    IConfigurationRoot config = new ConfigurationBuilder()
        //       .SetBasePath(Directory.GetCurrentDirectory())
        //       .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        //       .AddJsonFile($"appsettings.Development.json", optional: false)
        //       .Build();

        //    _transactionSvc = scpFactory.ServiceProvider.GetService<ITransactionService>();
        //    _propertySvc = scpFactory.ServiceProvider.GetService<IPropertyService>();
        //    _roomSvc = scpFactory.ServiceProvider.GetService<IRoomService>();
        //    _renterSvc = scpFactory.ServiceProvider.GetService<IRenterService>();
        //}

        //[Theory]
        //[InlineData("Renter 1", "renter1@gmail.com", "Address 1", "12345678901", "Job 1", 4)]
        //[InlineData("Renter 2", "renter2@gmail.com", "Address 2", "12345678902", "Job 2", 4)]
        //[InlineData("Renter 3", "renter1@gmail.com", "Address 1", "12345678901", "Job 1", 4)]
        //[InlineData("Renter 4", "renter2@gmail.com", "Address 2", "12345678902", "Job 2", 4)]
        //[InlineData("Renter 5", "renter3@gmail.com", "Address 3", "12345678903", "Job 3", 5)]
        //[InlineData("Renter 6", "renter4@gmail.com", "Address 4", "12345678904", "Job 4", 5)]
        //[InlineData("Renter 7", "renter3@gmail.com", "Address 3", "12345678903", "Job 3", 5)]
        //[InlineData("Renter 8", "renter4@gmail.com", "Address 4", "12345678904", "Job 4", 5)]
        //[InlineData("Renter 9", "renter5@gmail.com", "Address 5", "12345678905", "Job 5", 6)]
        //[InlineData("Renter 10", "renter6@gmail.com", "Address 6", "12345678906", "Job 6", 6)]
        //[InlineData("Renter 11", "renter5@gmail.com", "Address 5", "12345678905", "Job 5", 6)]
        //[InlineData("Renter 12", "renter6@gmail.com", "Address 6", "12345678906", "Job 6", 6)]
        //public async Task AddRenterAsync_Test(string name, string email, string address, string contactno, string profession, long propertyid)
        //{
        //    AddRenterCommandValidator validator = new AddRenterCommandValidator();

        //    AddRenterCommand addRenterCmd = new AddRenterCommand
        //    {
        //        Name = name,
        //        Email = email,
        //        Address = address,
        //        ContactNo = contactno,
        //        Profession = profession,
        //        PropertyId = propertyid
        //    };

        //    var result = validator.Validate(addRenterCmd);

        //    if (result.IsValid)
        //    {
        //        AddRenterModel renter = new AddRenterModel
        //        {
        //            Name = addRenterCmd.Name,
        //            Email = addRenterCmd.Email,
        //            Address = addRenterCmd.Address,
        //            ContactNo = addRenterCmd.ContactNo,
        //            Profession = addRenterCmd.Profession,
        //            PropertyId = addRenterCmd.PropertyId
        //        };

        //        var response = await _renterSvc.AddRenterAsync(renter);
        //        response.Status.Should().BeTrue();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData(1, 2)]
        //[InlineData(1, 3)]
        //public async Task GetRenterInfoAsync_Test(long propid, long renterid)
        //{
        //    GetRenterInfoQueryValidator validator = new GetRenterInfoQueryValidator();

        //    var getProperty = new GetRenterInfoQuery(propid, renterid);

        //    var result = validator.Validate(getProperty);

        //    if (result.IsValid)
        //    {
        //        RenterInfoDTO renter = await _renterSvc.GetRenterInfoAsync(propid, renterid);
        //        renter.Should().NotBeNull();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData(4)]
        //[InlineData(5)]
        //[InlineData(6)]
        //public async Task GetRentersAsync_Test(long propid)
        //{
        //    GetRentersQueryValidator validator = new GetRentersQueryValidator();

        //    var getProperty = new GetRentersQuery(propid);

        //    var result = validator.Validate(getProperty);

        //    if (result.IsValid)
        //    {
        //        IEnumerable<RenterListDTO> renters = await _renterSvc.GetRentersPerPropertyAsync(propid);
        //        renters.Should().NotBeNull();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData(4, 1, 10031)]
        //[InlineData(4, 2, 10034)]
        //[InlineData(4, 2, 10035)]
        //[InlineData(4, 3, 10039)]
        //[InlineData(4, 7, 10039)]
        //public async Task CheckInRenterAsync_Test(long propertyid, long roomid, long renterid)
        //{
        //    CheckinRenterCommandValidator validator = new CheckinRenterCommandValidator();

        //    CheckinRenterCommand checkinRenterCmd = new CheckinRenterCommand
        //    {
        //        PropId = propertyid,
        //        RoomId = roomid,
        //        RenterId = renterid
        //    };

        //    var result = validator.Validate(checkinRenterCmd);

        //    if (result.IsValid)
        //    {
        //        PropertyInfoDTO propInfo = await _propertySvc.GetPropertyInfoAsync(checkinRenterCmd.PropId);
        //        RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(propInfo.Id, checkinRenterCmd.RoomId);
        //        RenterInfoDTO renterInfo = await _renterSvc.GetRenterInfoAsync(propInfo.Id, checkinRenterCmd.RenterId);

        //        if (propInfo != null && roomInfo != null && renterInfo != null)
        //        {
        //            if (roomInfo.OccupiedBeds < roomInfo.TotalBeds)
        //            {
        //                bool isCheckedIn = await _renterSvc.GetRenterCheckinStatusAsync(checkinRenterCmd.PropId, checkinRenterCmd.RenterId);

        //                if (!isCheckedIn)
        //                {
        //                    CheckinModel checkin = new CheckinModel
        //                    {
        //                        RenterId = renterid,
        //                        RoomId = roomid,
        //                        PropertyId = propertyid
        //                    };

        //                    var checkinResult = await _transactionSvc.AddCheckInTransactionAsync(checkin);

        //                    if (checkinResult.Status)
        //                    {
        //                        RenterInfoDTO rentrInfo = await _renterSvc.GetRenterInfoAsync(propInfo.Id, checkinRenterCmd.RenterId);

        //                        InvoiceModel invoice = _transactionSvc.GenerateInvoice(roomInfo.Price, roomInfo.MonthsAdvance, roomInfo.MonthsDeposit, checkinRenterCmd.RenterId, Convert.ToDateTime(rentrInfo.CheckIn));
        //                        var resp = await _transactionSvc.SaveInvoiceAsync(invoice);
        //                        resp.Status.Should().BeTrue();
        //                    }
        //                }
        //                else
        //                    isCheckedIn.Should().BeFalse();
        //            }
        //            else
        //                Assert.Equal(roomInfo.TotalBeds, roomInfo.OccupiedBeds);
        //        }
        //        else
        //        {
        //            propInfo.Should().NotBeNull();
        //            roomInfo.Should().NotBeNull();
        //            renterInfo.Should().NotBeNull();
        //        }
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData(4, 1, 10031)]
        //[InlineData(4, 2, 10034)]
        //[InlineData(4, 2, 10035)]
        //[InlineData(4, 3, 10039)]
        //public async Task CheckoutRenterAsync_Test(long propertyid, long roomid, long renterid)
        //{
        //    CheckoutRenterCommandValidator validator = new CheckoutRenterCommandValidator();

        //    CheckoutRenterCommand checkinRenterCmd = new CheckoutRenterCommand
        //    {
        //        PropId = propertyid,
        //        RoomId = roomid,
        //        RenterId = renterid
        //    };

        //    var result = validator.Validate(checkinRenterCmd);

        //    if (result.IsValid)
        //    {
        //        PropertyInfoDTO propInfo = await _propertySvc.GetPropertyInfoAsync(propertyid);
        //        RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(propInfo.Id, roomid);
        //        RenterInfoDTO renterInfo = await _renterSvc.GetRenterInfoAsync(propInfo.Id, renterid);

        //        if (propInfo != null && roomInfo != null && renterInfo != null)
        //        {
        //            var isDeleted = await _renterSvc.DeleteRenterAsync(propertyid, renterid);
        //            await _roomSvc.UpdateBedCountAsync(propertyid, roomid, false);
        //            var isUpdated = await _transactionSvc.UpdateRenterInvoicesStatusAsync(renterid);

        //            isDeleted.Status.Should().BeTrue();
        //            isUpdated.Status.Should().BeTrue();
        //        }
        //        else
        //        {
        //            propInfo.Should().NotBeNull();
        //            roomInfo.Should().NotBeNull();
        //            renterInfo.Should().NotBeNull();
        //        }
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Validator;
using RentalsCoreApi31.Infrastructure.Operations.Property.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Property.Queries;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using System;
using System.IO;
using Xunit;
using FluentAssertions;

namespace RentalsCoreApi31.Tests.Services
{
    public class PropertyServiceTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private IPropertyService _propertySvc;
        private WebApplicationFactory<Startup> _svcFactory;

        public PropertyServiceTest(WebApplicationFactory<Startup> factory)
        {
            _svcFactory = factory;
            var scpFactory = _svcFactory.Services.GetService<IServiceScopeFactory>().CreateScope();

            IConfigurationRoot config = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appsettings.Development.json", optional: false)
               .Build();

            _propertySvc = scpFactory.ServiceProvider.GetService<IPropertyService>();
        }

        [Theory]
        [InlineData(1, "U1 Apartment 1", "U1 Address 1", "City 1", "11234567890", 10)]
        [InlineData(1, "U2 Apartment 2", "U2 Address 2", "City 1", "21234567890", 20)]
        [InlineData(1, "U3 Apartment 3", "U3 Address 3", "City 1", "31234567890", 30)]
        [InlineData(1, "U3 Apartment 3", "U4 Address 4", "City 1", "41234567890", 30)]
        [InlineData(0, "", "", "", "9638527410231540", 0)]
        public async Task AddPropertyAsync_Test(long userid, string name, string address, string city, string contact, int rooms)
        {
            AddPropertyCommandValidator validator = new AddPropertyCommandValidator();

            AddPropertyCommand addPropertyCmd = new AddPropertyCommand
            {
                Name = name,
                Address = address,
                City = city,
                ContactNo = contact,
                TotalRooms = rooms,
                UserId = userid
            };

            var result = validator.Validate(addPropertyCmd);

            if (result.IsValid)
            {
                var isPropertyExists = await _propertySvc.IsPropertyExistsAsync(addPropertyCmd.Name);

                if (!isPropertyExists.Status) isPropertyExists.Status.Should().BeTrue();

                AddPropertyModel property = new AddPropertyModel
                {
                    Name = addPropertyCmd.Name,
                    Address = addPropertyCmd.Address,
                    City = addPropertyCmd.City,
                    ContactNo = addPropertyCmd.ContactNo,
                    TotalRooms = addPropertyCmd.TotalRooms,
                    UserId = addPropertyCmd.UserId
                };

                var response = await _propertySvc.AddPropertyAsync(property);
                response.Status.Should().BeTrue();
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
                }

                result.Errors.Should().BeNullOrEmpty();
            }
        }

        [Theory]
        [InlineData(10016)]
        [InlineData(10017)]
        [InlineData(10018)]
        [InlineData(1)]
        public async Task GetPropertyInfoAsync_Test(long propid)
        {
            GetPropertyInfoQueryValidator validator = new GetPropertyInfoQueryValidator();

            GetPropertyInfoQuery getProperty = new GetPropertyInfoQuery(propid);

            var result = validator.Validate(getProperty);

            if (result.IsValid)
            {
                var property = await _propertySvc.GetPropertyInfoAsync(propid);
                property.Data.Should().NotBeNull();
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
                }

                result.Errors.Should().BeNullOrEmpty();
            }
        }

        [Theory]
        [InlineData(1)]
        public async Task GetPropertiesAsync_Test(long userid)
        {
            GetPropertiesQueryValidator validator = new GetPropertiesQueryValidator();

            GetPropertiesQuery getPropertiesQuery = new GetPropertiesQuery(userid);

            var result = validator.Validate(getPropertiesQuery);

            if (result.IsValid)
            {
                var properties = await _propertySvc.GetPropertiesAsync(userid);
                properties.Data.Should().HaveCountGreaterThan(0);
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
                }

                result.Errors.Should().BeNullOrEmpty();
            }
        }

        //[Theory]
        //[InlineData(1, "U1 Apartment Name 1", "U1 Upd Addr", "City 1", "11265437890", 50, 5)]
        //[InlineData(2, "U2 Apartment 2", "U2 Updated Address 1", "City 1", "20987654321", 50, 5)]
        //[InlineData(3, "U3 Apartment 3", "U3 Updated", "U3 City 1 Updated", "85423654124", 50, 5)]
        //public async Task UpdatePropertyInfoAsync_Test(long propid, string name, string address, string city, string contactNo, int rooms, long userid)
        //{
        //    UpdatePropertyInfoCommandValidator validator = new UpdatePropertyInfoCommandValidator();

        //    UpdatePropertyInfoCommand updPropertyCmd = new UpdatePropertyInfoCommand
        //    {
        //        PropertyId = propid,
        //        Name = name,
        //        Address = address,
        //        City = city,
        //        ContactNo = contactNo,
        //        TotalRooms = rooms,
        //        UserId = userid
        //    };

        //    var result = validator.Validate(updPropertyCmd);

        //    if (result.IsValid)
        //    {
        //        UpdatePropertyModel property = new UpdatePropertyModel
        //        {
        //            PropertyId = updPropertyCmd.PropertyId,
        //            Name = updPropertyCmd.Name,
        //            Address = updPropertyCmd.Address,
        //            City = updPropertyCmd.City,
        //            ContactNo = updPropertyCmd.ContactNo,
        //            TotalRooms = updPropertyCmd.TotalRooms,
        //            UserId = updPropertyCmd.UserId
        //        };

        //        var response = await _propertySvc.UpdatePropertyInfoAsync(property);
        //        response.Status.Should().BeTrue();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}
    }
}

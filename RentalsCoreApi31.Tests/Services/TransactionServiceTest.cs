﻿using RentalsCoreApi31.Infrastructure.Operations.Transactions.Services;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;

using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks;
using System;
using FluentAssertions;

namespace RentalsCoreApi31.Tests.Services
{
    public class TransactionServiceTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private ITransactionService _transactionSvc;
        private IRenterService _renterSvc;
        private IRoomService _roomSvc;

        private WebApplicationFactory<Startup> _svcFactory;

        public TransactionServiceTest(WebApplicationFactory<Startup> factory)
        {
            _svcFactory = factory;
            var scpFactory = _svcFactory.Services.GetService<IServiceScopeFactory>().CreateScope();

            IConfigurationRoot config = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appsettings.Development.json", optional: false)
               .Build();

            _transactionSvc = scpFactory.ServiceProvider.GetService<ITransactionService>();
            _renterSvc = scpFactory.ServiceProvider.GetService<IRenterService>();
            _roomSvc = scpFactory.ServiceProvider.GetService<IRoomService>();
        }

        // TODO: Implement as CRON job
        //[Fact]
        //public async Task GenerateInvoiceAsync_Test()
        //{
        //    Guid propId = Guid.Parse("85079C2E-7E48-4872-875B-9BFD401E4FA8");
        //    Guid renterId = Guid.Parse("663CAC13-52DF-457F-AA54-F5E9E4C0408B");
        //    Guid roomId = Guid.Parse("2DE9A545-F1D8-49B6-8869-4B7613F5EA88");

        //    DateTime lastBillingDueDate = await _renterSvc.GetRenterLastBillingDueDateAsync(renterId);

        //    RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(propId, roomId);
        //    // TODO: Call service which calculates total fee of all additional appliances
        //    decimal amountDue = roomInfo.Price; // roomInfo.Price + sum of total fee for additional appliances
        //    decimal balance = await _renterSvc.GetRenterBalanceAsync(propId, renterId);
        //    decimal remBalance = amountDue + balance;

        //    InvoiceModel invoice = _transactionSvc.GenerateInvoice(remBalance, renterId, lastBillingDueDate);
        //    var result = await _transactionSvc.SaveInvoiceAsync(invoice);
        //    result.Status.Should().BeTrue();
        //}

        //[Fact]
        //public async Task AddTransactionAsync_Test()
        //{
        //    Guid propId = Guid.Parse("85079C2E-7E48-4872-875B-9BFD401E4FA8");
        //    Guid renterId = Guid.Parse("E5430E30-5EDF-4115-AEE0-F514DD11CC84");

        //    // TODO: Balance calculation if renter will pay before due date
        //    decimal amountDue = await _renterSvc.GetLatestInvoiceAsync(renterId);
        //    decimal amountPd = 5000;
        //    decimal balance = amountDue - amountPd;

        //    var transaction = new AddTransactionHistoryModel
        //    {
        //        DatePaid = DateTime.Now,
        //        AmountDue = amountDue,
        //        AmountPaid = amountPd,
        //        PaymentFor = "Rent",
        //        Balance = balance,
        //        PropertyId = propId,
        //        RenterId = renterId
        //    };

        //    var result = await _transactionSvc.SaveTransactionAsync(transaction);
        //    result.Status.Should().BeTrue();
        //}
    }
}

using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.User.Services;
using RentalsCoreApi31.Infrastructure.Operations.User.Validator;
using RentalsCoreApi31.Infrastructure.Operations.User.Commands;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using System.IO;
using System;
using FluentAssertions;
using Xunit;

namespace RentalsCoreApi31.Tests.Services
{
    public class AccountServiceTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        //private IAccountService _accountSvc;
        //private WebApplicationFactory<Startup> _svcFactory;

        //public AccountServiceTest(WebApplicationFactory<Startup> factory)
        //{
        //    _svcFactory = factory;
        //    var scpFactory = _svcFactory.Services.GetService<IServiceScopeFactory>().CreateScope();

        //    IConfigurationRoot config = new ConfigurationBuilder()
        //       .SetBasePath(Directory.GetCurrentDirectory())
        //       .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        //       .AddJsonFile($"appsettings.Development.json", optional: false)
        //       .Build();

        //    _accountSvc = scpFactory.ServiceProvider.GetService<IAccountService>();
        //}

        //[Theory]
        //[InlineData("Admin", "admin@gmail.com", "Demo1231!", "Admin")]
        //[InlineData("", "", "", "")]
        //public async Task RegisterUserAsync_Test(string name, string email, string password, string role)
        //{
        //    CreateUserCommandValidator validator = new CreateUserCommandValidator();

        //    var createUserCmd = new CreateUserCommand
        //    {
        //        Name = name,
        //        Email = email,
        //        Password = password,
        //        Role = role
        //    };

        //    var result = validator.Validate(createUserCmd);

        //    if (result.IsValid)
        //    {
        //        UserRegisterModel user = new UserRegisterModel
        //        {
        //            Name = createUserCmd.Name,
        //            Email = createUserCmd.Email,
        //            Password = createUserCmd.Password,
        //            Role = createUserCmd.Role
        //        };

        //        var isRegistered = await _accountSvc.RegisterUserAsync(user);
        //        isRegistered.Status.Should().BeTrue();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}

        //[Theory]
        //[InlineData("admin@gmail.com", "Demo1231!")]
        //[InlineData("user@gmail.com", "demo123!")]
        //[InlineData("user1@gmail.com", "demo1231!")]
        //[InlineData("user2@gmail.com", "demo1232!")]
        //public async Task LoginUserAsync_Test(string email, string password)
        //{
        //    LoginUserCommandValidator validator = new LoginUserCommandValidator();

        //    var loginUserCommand = new LoginUserCommand
        //    {
        //        Email = email,
        //        Password = password
        //    };

        //    var result = validator.Validate(loginUserCommand);

        //    if (result.IsValid)
        //    {
        //        var user = new UserLoginModel
        //        {
        //            Email = loginUserCommand.Email,
        //            Password = loginUserCommand.Password
        //        };

        //        TokenModel token = await _accountSvc.VerifyUserAsync(user);

        //        if (token != null)
        //            token.AccessToken.Should().NotBeNullOrEmpty();
        //        else
        //            token.AccessToken.Should().NotBeNullOrEmpty();
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            Console.WriteLine($"Property: {error.PropertyName}\nError: {error.ErrorMessage}\n");
        //        }

        //        result.Errors.Should().BeNullOrEmpty();
        //    }
        //}
    }
}

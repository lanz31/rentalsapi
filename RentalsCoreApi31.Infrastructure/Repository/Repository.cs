﻿//using RentalsCoreApi31.Core.BusinessModels.DTO;
//using RentalsCoreApi31.Core.BusinessModels.Implementation;
//using RentalsCoreApi31.Core.BusinessModels.Interface;
//using RentalsCoreApi31.Core.Models;
//using RentalsCoreApi31.Core.Repository;
//using RentalsCoreApi31.Core.Services;
//using RentalsCoreApi31.Infrastructure.DataContext;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace RentalsCoreApi31.Infrastructure.Repository
{
    public class Repository //: IRepository
    {
        //private DatabaseContext _db;
        //private IResponseModel _response;
        //private IConfiguration _config;
        //private ILoggerService _loggerService;

        //public Repository(
        //    DatabaseContext db,
        //    IResponseModel response,
        //    IConfiguration config,
        //    ILoggerService loggerService)
        //{
        //    _db = db;
        //    _response = response;
        //    _config = config;
        //    _loggerService = loggerService;
        //}

        #region Property Repository
        //public async Task<IResponseModel> AddPropertyAsync(PropertyModel model)
        //{
        //    try
        //    {
        //        var property = new RealEstateProperty
        //        {
        //            Id = Guid.NewGuid(),
        //            UserId = model.UserId,
        //            Name = model.Name,
        //            Address = model.Address,
        //            City = model.City,
        //            ContactNo = model.ContactNo,
        //            Owner = model.Owner,
        //            TotalRooms = model.TotalRooms
        //        };

        //        _db.RealEstateProperties.Add(property);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Property added successfully.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Add New Property", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding property.";

        //        return _response;
        //    }
        //}

        //public async Task<List<PropertiesTermsDTO>> GetOwnerPropertiesAsync(Guid userid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var userId = Guid.Parse(userid.ToString());

        //            var properties = await con.QueryAsync<PropertiesTermsDTO>("sp_GetOwnerPropertiesWithTerms", new { UserId = userId }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return properties.AsList() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Owner Properties", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<PropertiesTermsDTO> GetPropertyInfoAsync(Guid userid, Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var properties = await con.QueryAsync<PropertiesTermsDTO>("sp_GetPropertyInfo", new { UserId = userid, PropertyId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return properties.SingleOrDefault() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Property Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<IResponseModel> UpdatePropertyInfoAsync(PropertyModel model)
        //{
        //    try
        //    {
        //        var property = await _db.RealEstateProperties.SingleOrDefaultAsync(p => p.UserId == model.UserId && p.Id == model.PropertyId);

        //        if (property != null)
        //        {
        //            property.Name = model.Name;
        //            property.Address = model.Address;
        //            property.City = model.City;
        //            property.ContactNo = model.ContactNo;
        //            property.Owner = model.Owner;
        //            property.TotalRooms = model.TotalRooms;

        //            _db.RealEstateProperties.Update(property);
        //            await _db.SaveChangesAsync();

        //            _response.Status = true;
        //            _response.Message = $"{model.Name} successfully updated.";

        //            return _response;
        //        }
        //        else
        //        {
        //            _response.Status = false;
        //            _response.Message = "No property found.";

        //            return _response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Update Property Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding property.";

        //        return _response;
        //    }
        //}

        //public async Task<decimal> GetPropertyTermsAsync(Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var terms = await con.QueryAsync<decimal>("sp_GetPropertyTerms", new { PropId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return terms?.SingleOrDefault() ?? 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Property Terms", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return 0;
        //    }
        //}

        //public async Task<IResponseModel> UpdatePropertyTermsAsync(PropertyTermsModel model)
        //{
        //    try
        //    {
        //        var settings = await _db.Settings.Where(s => s.RoomId == model.RoomId).SingleOrDefaultAsync();
        //        settings.MonthAdvance = model.MonthAdvance;
        //        settings.MonthDeposit = model.MonthDeposit;

        //        _db.Settings.Update(settings);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Successfully updated property terms";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Update Property Terms", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error updating property terms.";

        //        return _response;
        //    }
        //}
        #endregion

        #region Room Repository
        //public async Task<IResponseModel> AddRoomTermsAsync(PropertyTermsModel model)
        //{
        //    try
        //    {
        //        var settings = new PropertySettings
        //        {
        //            Id = Guid.NewGuid(),
        //            MonthDeposit = model.MonthDeposit,
        //            MonthAdvance = model.MonthAdvance,
        //            RoomId = model.RoomId
        //        };

        //        _db.Settings.Add(settings);
        //        var result = await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Property Terms added successfully.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Add Property Terms", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding property terms.";

        //        return _response;
        //    }
        //}

        //public async Task<IResponseModel> AddRoomTypesAsync(RoomTypeModel model)
        //{
        //    try
        //    {
        //        var roomType = new RoomTypes
        //        {
        //            Id = Guid.NewGuid(),
        //            Type = model.Type,
        //            Price = model.Price,
        //            PropertyId = model.PropertyId
        //        };

        //        _db.RoomTypes.Add(roomType);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "New room type added.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Add Room Type", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding room type.";

        //        return _response;
        //    }
        //}

        //public async Task<List<RoomTypeModel>> GetRoomTypesPerPropertyAsync(Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var roomTypes = await con.QueryAsync<RoomTypeModel>("sp_GetRoomTypesPerProperty", new { PropertyId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return roomTypes.Count() > 0 ? roomTypes.AsList() : null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Property Room Types", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<List<RoomsListInfoDTO>> GetRoomsPerPropertyAsync(Guid userid, Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var roomList = await con.QueryAsync<RoomsListInfoDTO>("sp_GetRoomsPerProperty", new { UserId = userid, PropertyId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return roomList.ToList() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Rooms Per Property", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<List<RoomPriceDTO>> GetRoomsWithPricesAsync(Guid userid, Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var roomPrices = await con.QueryAsync<RoomPriceDTO>("sp_GetRoomsWithPrices", new { UserId = userid, PropertyId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return roomPrices.ToList() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Rooms With Prices", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<RoomPriceDTO> GetRoomInfoAsync(Guid userid, Guid propertyid, Guid roomid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var roomInfo = await con.QueryAsync<RoomPriceDTO>("sp_GetRoomInfo", new { UserId = userid, PropId = propertyid, RoomId = roomid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return roomInfo.SingleOrDefault() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Room Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<AvailableBedsDTO> GetAvailableBedsPerRoomAsync(Guid userid, Guid propertyid, Guid roomid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var availableBeds = await con.QueryAsync<AvailableBedsDTO>("sp_GetAvailableBedsPerRoom", new { UserId = userid, PropId = propertyid, RoomId = roomid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return availableBeds.SingleOrDefault() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Available Beds Per Room", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}
        #endregion

        #region Room Feature Repository
        //public async Task<List<RoomFeaturesDTO>> GetRoomFeaturesAsync(Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var features = await con.QueryAsync<RoomFeaturesDTO>("sp_GetRoomFeatures", new { PropertyId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return features.ToList() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Rooms Features", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}
        #endregion

        #region Renter Repository
        //public async Task<RenterInfoDTO> GetRenterInfoAsync(Guid propertyid, Guid renterid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var renterInfo = await con.QueryAsync<RenterInfoDTO>("sp_GetRenterInfo", new { PropId = propertyid, RenterId = renterid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return renterInfo.SingleOrDefault() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renter Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<List<RenterListDTO>> GetRentersPerPropertyAsync(Guid propertyid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var renters = await con.QueryAsync<RenterListDTO>("sp_GetRoomFeatures", new { PropertyId = propertyid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return renters.ToList() ?? null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renters Per Property", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}
        #endregion
    }
}

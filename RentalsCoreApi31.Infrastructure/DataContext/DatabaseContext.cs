﻿using RentalsCoreApi31.Core.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace RentalsCoreApi31.Infrastructure.DataContext
{
    public class DatabaseContext : IdentityDbContext<ApplicationUser, ApplicationRole, long>
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<RealEstateProperty> Properties { get; set; }
        public DbSet<Renter> Renter { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<TransactionHistory> Transactions { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomTypes> RoomTypes { get; set; }
        public DbSet<RoomFeatures> RoomFeatures { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            #region User Configuration
            //builder.Entity<ApplicationUser>()
            //    .HasData(new ApplicationUser
            //    {
            //        Id = 1,
            //        Name = "Admin",
            //        UserName = "admin@gmail.com",
            //        Email = "admin@gmail.com",
            //        EmailConfirmed = true
            //    });
            #endregion

            #region Role Configuration
            builder.Entity<ApplicationRole>().HasData(
                new ApplicationRole { Id = 1, Name = "Admin", NormalizedName = "ADMIN" }
            );
            #endregion

            #region Property Configuration
            builder.Entity<RealEstateProperty>(entity =>
            {
                entity.ToTable("Properties").HasKey(p => p.Id);
                entity.HasOne(u => u.User).WithMany(p => p.Properties).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Cascade);

                entity.Property(p => p.Name).IsRequired();
                entity.Property(p => p.Address).IsRequired();
                entity.Property(p => p.City).IsRequired();
                entity.Property(p => p.ContactNo).IsRequired();
                entity.Property(p => p.TotalRooms).IsRequired();
            });
            #endregion

            #region Renter Configuration
            builder.Entity<Renter>(entity =>
            {
                entity.ToTable("Renters").HasKey(p => p.Id);
                entity.HasOne(p => p.Property).WithMany(r => r.Renters).HasForeignKey(p => p.PropertyId).OnDelete(DeleteBehavior.Cascade);

                entity.Property(p => p.Name).IsRequired();
                entity.Property(p => p.Email).IsRequired();
                entity.Property(p => p.Address).IsRequired();
                entity.Property(p => p.ContactNo).IsRequired();
                entity.Property(p => p.Profession).IsRequired();
                entity.Property(p => p.CheckIn).IsRequired(false);
                entity.Property(p => p.CheckOut).IsRequired(false);
                entity.Property(p => p.RoomId).IsRequired(false);
                entity.Property(p => p.Status).IsRequired();
            });
            #endregion

            #region Rooms Configuration
            builder.Entity<Room>(entity =>
            {
                entity.ToTable("Rooms").HasKey(p => p.Id);
                entity.HasOne(p => p.Property).WithMany(r => r.Rooms).HasForeignKey(p => p.PropertyId).OnDelete(DeleteBehavior.Cascade);

                entity.Property(p => p.Name).IsRequired();
                entity.Property(p => p.TotalBeds).IsRequired();
                entity.Property(p => p.OccupiedBeds).IsRequired();
            });
            #endregion

            #region Room Types Configuration
            builder.Entity<RoomTypes>(entity =>
            {
                entity.ToTable("RoomTypes").HasKey(p => p.Id);
                entity.HasOne(p => p.Property).WithMany(r => r.RoomTypes).HasForeignKey(p => p.PropertyId).OnDelete(DeleteBehavior.Cascade);
                entity.HasMany(r => r.Rooms).WithOne(rt => rt.RoomType).HasForeignKey(p => p.RoomTypeId).OnDelete(DeleteBehavior.ClientSetNull);

                entity.Property(p => p.Type).IsRequired();
                entity.Property(p => p.Price).IsRequired();
                entity.Property(p => p.MonthsAdvance).IsRequired();
                entity.Property(p => p.MonthsDeposit).IsRequired();
            });
            #endregion

            #region Invoice Configurataion
            builder.Entity<Invoice>(entity =>
            {
                entity.ToTable("Invoice").HasKey(p => p.Id);
                entity.HasOne(r => r.Renter).WithMany(p => p.Invoices).HasForeignKey(r => r.RenterId).OnDelete(DeleteBehavior.ClientSetNull);

                entity.Property(p => p.DateCreated).IsRequired();
                entity.Property(p => p.AmountDue).IsRequired();
                entity.Property(p => p.BillingDue).IsRequired();
                entity.Property(p => p.Status).IsRequired();
            });
            #endregion

            #region Transaction History Configurataion
            builder.Entity<TransactionHistory>(entity =>
            {
                entity.ToTable("Transactions").HasKey(p => p.Id);
                entity.HasOne(r => r.Property).WithMany(p => p.Transactions).HasForeignKey(r => r.PropertyId).OnDelete(DeleteBehavior.ClientSetNull);
                entity.HasOne(r => r.Renter).WithMany(p => p.Transactions).HasForeignKey(r => r.RenterId).OnDelete(DeleteBehavior.ClientSetNull);

                entity.Property(p => p.AmountDue).IsRequired();
                entity.Property(p => p.AmountPaid).IsRequired();
                entity.Property(p => p.PaymentFor).IsRequired();
                entity.Property(p => p.Balance).IsRequired();
            });
            #endregion

            #region Room Features Configuration
            builder.Entity<RoomFeatures>(entity =>
            {
                entity.ToTable("RoomFeatures").HasKey(p => p.Id);
                entity.HasOne(r => r.Room).WithMany(r => r.Features).HasForeignKey(r => r.RoomId).OnDelete(DeleteBehavior.Cascade);
                
                entity.Property(p => p.Name).IsRequired();
            });
            #endregion
        }
    }
}

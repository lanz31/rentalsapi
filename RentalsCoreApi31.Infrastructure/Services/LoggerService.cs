﻿using RentalsCoreApi31.Core.Services;

using System.IO;
using System;

namespace RentalsCoreApi31.Infrastructure.Services
{
    public class LoggerService : ILoggerService
    {
        public void Log(string feature, string innerex, string message, string stacktrace)
        {
            var logFilePath = @"G:\Repository\RentalsCoreApi31\RentalsCoreApi31\Logs\";
            var filename = "Log_" + Guid.NewGuid().ToString().Replace("-", "") + DateTime.UtcNow.ToString().Replace(" ", "").Replace("/", "").Replace(":", "");
            var filepath = Path.Combine(logFilePath + filename + ".txt");

            if (!File.Exists(filepath))
            {
                using (StreamWriter writer = new StreamWriter(filepath, true))
                {
                    writer.WriteLine("Feature:\n" + feature + "\n\n" + "Inner Exception:\n" + innerex + "\n\n" + "Message:\n" + message + "\n\n" + "Stack Trace:\n\n" + stacktrace);
                    writer.Close();
                }
            }
        }
    }
}

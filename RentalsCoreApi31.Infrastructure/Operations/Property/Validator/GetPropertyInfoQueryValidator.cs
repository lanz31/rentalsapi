﻿using RentalsCoreApi31.Infrastructure.Operations.Property.Queries;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Validator
{
    public class GetPropertyInfoQueryValidator : AbstractValidator<GetPropertyInfoQuery>
    {
        public GetPropertyInfoQueryValidator()
        {
            RuleFor(x => x.PropId).NotEmpty().NotNull().WithMessage("Property id field is mandatory.");
        }
    }
}

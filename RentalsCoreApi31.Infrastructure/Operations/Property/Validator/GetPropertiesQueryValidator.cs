﻿using RentalsCoreApi31.Infrastructure.Operations.Property.Queries;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Validator
{
    public class GetPropertiesQueryValidator : AbstractValidator<GetPropertiesQuery>
    {
        public GetPropertiesQueryValidator()
        {
            RuleFor(x => x.UserId).NotEmpty().NotNull().WithMessage("User id field is mandatory.");
        }
    }
}

﻿using RentalsCoreApi31.Infrastructure.Operations.Property.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Validator
{
    public class UpdatePropertyInfoCommandValidator : AbstractValidator<UpdatePropertyInfoCommand>
    {
        public UpdatePropertyInfoCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Name field is mandatory.");

            RuleFor(x => x.Address)
                .NotEmpty()
                .NotNull()
                .WithMessage("Address field is mandatory.");

            RuleFor(x => x.City)
                .NotEmpty()
                .NotNull()
                .WithMessage("City field is mandatory.");

            RuleFor(x => x.ContactNo)
                .MaximumLength(15)
                .NotEmpty()
                .NotNull()
                .WithMessage("Contact # field is mandatory.");

            RuleFor(x => x.TotalRooms)
                .GreaterThanOrEqualTo(1)
                .WithMessage("Room count must be at least 1.")
                .NotEmpty()
                .NotNull()
                .WithMessage("# of Rooms field is mandatory.");

            RuleFor(x => x.UserId)
                .NotEmpty()
                .NotNull()
                .WithMessage("User id field is mandatory.");

            RuleFor(x => x.PropertyId)
                .NotEmpty()
                .NotNull()
                .WithMessage("Property id field is mandatory.");
        }
    }
}

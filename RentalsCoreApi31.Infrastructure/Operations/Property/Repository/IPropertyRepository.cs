﻿using RentalsCoreApi31.Infrastructure.DataContext;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Core.Services;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using System;
using System.Threading.Tasks;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Repository
{
    public interface IPropertyRepository
    {
        Task<RealEstateProperty> AddPropertyAsync(RealEstateProperty obj);
        Task<PropertyInfoDTO> GetPropertyInfoByNameAsync(string name);
        Task<PropertyInfoDTO> GetPropertyInfoByIdAsync(long pid);
        Task<IEnumerable<PropertyInfoDTO>> GetPropertiesAsync(long uid);
        Task<RealEstateProperty> UpdatePropertyInfoAsync(RealEstateProperty obj);
    }

    public class PropertyRepository : IPropertyRepository
    {
        private DatabaseContext _db;
        private ILoggerService _loggerService;
        private IConfiguration _config;

        public PropertyRepository(
            DatabaseContext db,
            ILoggerService loggerService,
            IConfiguration config)
        {
            _db = db;
            _loggerService = loggerService;
            _config = config;
        }

        public async Task<RealEstateProperty> AddPropertyAsync(RealEstateProperty obj)
        {
            try
            {
                _db.Properties.Add(obj);
                await _db.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Add New Property", ex.InnerException.Message, ex.Message, ex.StackTrace);
                return null;
            }
        }

        public async Task<PropertyInfoDTO> GetPropertyInfoByNameAsync(string name)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var property = await con.QueryAsync<PropertyInfoDTO>("sp_GetPropertyByName", new { Name = name }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return property.Count() > 0 ? property.FirstOrDefault() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Property Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<PropertyInfoDTO> GetPropertyInfoByIdAsync(long pid)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var property = await con.QueryAsync<PropertyInfoDTO>("sp_GetPropertyInfo", new { PropId = pid }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return property.Count() > 0 ? property.FirstOrDefault() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Owner Properties", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<IEnumerable<PropertyInfoDTO>> GetPropertiesAsync(long uid)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var properties = await con.QueryAsync<PropertyInfoDTO>("sp_GetProperties", new { UserId = uid }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return properties.Count() > 0 ? properties : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Property Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<RealEstateProperty> UpdatePropertyInfoAsync(RealEstateProperty obj)
        {
            try
            {
                _db.Properties.Update(obj);
                await _db.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Update Property Info", ex.InnerException.Message, ex.Message, ex.StackTrace);
                return null;
            }
        }
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Repository;

using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Services
{
    public interface IPropertyService
    {
        Task<IResponseModel<Response>> AddPropertyAsync(AddPropertyModel model);
        Task<IResponseModel<Response>> IsPropertyExistsAsync(string name);
        Task<IResponseModel<PropertyInfoDTO>> GetPropertyInfoAsync(long pid);
        Task<IResponseModel<IEnumerable<PropertyInfoDTO>>> GetPropertiesAsync(long uid);
        //Task<IResponseModel> UpdatePropertyInfoAsync(UpdatePropertyModel model);
    }

    public class PropertyService : IPropertyService
    {
        private IPropertyRepository _repo;
        private IResponseService<Response> _responseSvc;
        private IResponseService<IEnumerable<PropertyInfoDTO>> _propertiesResp;
        private IResponseService<PropertyInfoDTO> _propInfoResp;

        public PropertyService(
            IPropertyRepository repo,
            IResponseService<Response> responseSvc,
            IResponseService<IEnumerable<PropertyInfoDTO>> propertiesResp,
            IResponseService<PropertyInfoDTO> propInfoResp)
        {
            _repo = repo;
            _responseSvc = responseSvc;
            _propertiesResp = propertiesResp;
            _propInfoResp = propInfoResp;
        }

        public async Task<IResponseModel<Response>> AddPropertyAsync(AddPropertyModel model)
        {
            RealEstateProperty property = new RealEstateProperty
            {
                UserId = model.UserId,
                Name = model.Name,
                Address = model.Address,
                City = model.City,
                ContactNo = model.ContactNo,
                TotalRooms = model.TotalRooms,
                DateCreated = DateTime.Now
            };

            RealEstateProperty prop = await _repo.AddPropertyAsync(property);

            return prop != null
                ? _responseSvc.Response(true, "Property added successfully.", HttpStatusCode.OK)
                : _responseSvc.Response(false, "Error adding property", HttpStatusCode.BadRequest);
        }

        public async Task<IResponseModel<Response>> IsPropertyExistsAsync(string name)
        {
            PropertyInfoDTO propInfo = await _repo.GetPropertyInfoByNameAsync(name);

            return propInfo != null
                ? _responseSvc.Response(false, "Property already exists.", HttpStatusCode.BadRequest)
                : _responseSvc.Response(true, "Property available", HttpStatusCode.OK);
        }

        public async Task<IResponseModel<PropertyInfoDTO>> GetPropertyInfoAsync(long pid)
        {
            PropertyInfoDTO propInfo = await _repo.GetPropertyInfoByIdAsync(pid);

            return propInfo != null
                ? _propInfoResp.Response(true, "", HttpStatusCode.OK, propInfo)
                : _propInfoResp.Response(false, "Property not found.", HttpStatusCode.BadRequest);
        }

        public async Task<IResponseModel<IEnumerable<PropertyInfoDTO>>> GetPropertiesAsync(long uid) =>
            _propertiesResp.Response(true, "", HttpStatusCode.OK, await _repo.GetPropertiesAsync(uid));

        //public async Task<IResponseModel> UpdatePropertyInfoAsync(UpdatePropertyModel model)
        //{
        //    var propInfo = await _repo.GetPropertyInfoByIdAsync(model.PropertyId);

        //    if (propInfo == null) return _responseSvc.Response(false, "Property not found.", HttpStatusCode.BadRequest);

        //    RealEstateProperty prop = await _repo.UpdatePropertyInfoAsync(new RealEstateProperty
        //    {
        //        Id = model.PropertyId,
        //        UserId = model.UserId,
        //        Name = model.Name,
        //        Address = model.Address,
        //        ContactNo = model.ContactNo,
        //        City = model.City,
        //        DateCreated = DateTime.Now,
        //        TotalRooms = model.TotalRooms
        //    });

        //    return prop != null
        //        ? _responseSvc.Response(true, "Property successfully updated.", HttpStatusCode.OK)
        //        : _responseSvc.Response(false, "Error updating property.", HttpStatusCode.BadRequest);
        //}
    }
}
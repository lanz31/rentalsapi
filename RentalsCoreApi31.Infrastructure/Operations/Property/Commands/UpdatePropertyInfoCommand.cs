﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Commands
{
    public class UpdatePropertyInfoCommand //: IRequest<IResponseModel>
    {
        public long PropertyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ContactNo { get; set; }
        public int TotalRooms { get; set; }
        public long UserId { get; set; }
    }
}

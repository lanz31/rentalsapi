﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System.Collections.Generic;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Queries
{
    public class GetPropertiesQuery : IRequest<IResponseModel<IEnumerable<PropertyInfoDTO>>>
    {
        public long UserId { get; set; }

        public GetPropertiesQuery(long userid)
        {
            UserId = userid;
        }
    }
}

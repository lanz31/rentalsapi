﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;

using MediatR;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Queries
{
    public class GetPropertyInfoQuery : IRequest<IResponseModel<PropertyInfoDTO>>
    {
        public long PropId { get; set; }

        public GetPropertyInfoQuery(long propid)
        {
            PropId = propid;
        }
    }
}

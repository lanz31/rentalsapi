﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.Property.Queries;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Handlers
{
    public class GetPropertyInfoQueryHandler : IRequestHandler<GetPropertyInfoQuery, IResponseModel<PropertyInfoDTO>>
    {
        private IPropertyService _propertySvc;

        public GetPropertyInfoQueryHandler(IPropertyService propertySvc)
        {
            _propertySvc = propertySvc;
        }

        public async Task<IResponseModel<PropertyInfoDTO>> Handle(GetPropertyInfoQuery request, CancellationToken cancellationToken)
            => await _propertySvc.GetPropertyInfoAsync(request.PropId) ?? null;
    }
}

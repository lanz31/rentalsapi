﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Queries;

using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Handlers
{
    public class GetPropertiesQueryHandler : IRequestHandler<GetPropertiesQuery, IResponseModel<IEnumerable<PropertyInfoDTO>>>
    {
        private IPropertyService _propertySvc;

        public GetPropertiesQueryHandler(IPropertyService propertySvc)
        {
            _propertySvc = propertySvc;
        }

        public async Task<IResponseModel<IEnumerable<PropertyInfoDTO>>> Handle(GetPropertiesQuery request, CancellationToken cancellationToken)
            => await _propertySvc.GetPropertiesAsync(request.UserId) ?? null;
    }
}

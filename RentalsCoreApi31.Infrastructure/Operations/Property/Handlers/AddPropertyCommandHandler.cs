﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;

namespace RentalsCoreApi31.Infrastructure.Operations.Property.Handlers
{
    public class AddPropertyCommandHandler : IRequestHandler<AddPropertyCommand, IResponseModel<Response>>
    {
        private IPropertyService _propertySvc;
        private IResponseService<Response> _responseSvc;

        public AddPropertyCommandHandler(IPropertyService propertySvc, IResponseService<Response> responseSvc)
        {
            _propertySvc = propertySvc;
            _responseSvc = responseSvc;
        }

        public async Task<IResponseModel<Response>> Handle(AddPropertyCommand request, CancellationToken cancellationToken)
        {
            var isPropertyExists = await _propertySvc.IsPropertyExistsAsync(request.Name);

            if (!isPropertyExists.Status) return _responseSvc.Response(isPropertyExists.Status, isPropertyExists.Message, isPropertyExists.StatusCode);

            AddPropertyModel property = new AddPropertyModel
            {
                UserId = request.UserId,
                Name = request.Name,
                Address = request.Address,
                City = request.City,
                ContactNo = request.ContactNo,
                TotalRooms = request.TotalRooms
            };

            return await _propertySvc.AddPropertyAsync(property);
        }
    }
}

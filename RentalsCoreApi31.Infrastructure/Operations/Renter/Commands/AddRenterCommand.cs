﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Commands
{
    public class AddRenterCommand //: IRequest<IResponseModel>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Profession { get; set; }
        public long PropertyId { get; set; }
    }
}

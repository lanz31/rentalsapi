﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Commands
{
    public class CheckoutRenterCommand //: IRequest<IResponseModel>
    {
        public long PropId { get; set; }
        public long RoomId { get; set; }
        public long RenterId { get; set; }
    }
}

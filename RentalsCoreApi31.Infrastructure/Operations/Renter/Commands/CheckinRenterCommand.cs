﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Commands
{
    public class CheckinRenterCommand //: IRequest<IResponseModel>
    {
        public long PropId { get; set; }
        public long RoomId { get; set; }
        public long RenterId { get; set; }
    }
}

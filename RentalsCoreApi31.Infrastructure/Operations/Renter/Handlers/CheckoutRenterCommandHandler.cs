﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Services;
using RentalsCoreApi31.Infrastructure.Operations.Transactions.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Handlers
{
    public class CheckoutRenterCommandHandler //: IRequestHandler<CheckoutRenterCommand, IResponseModel>
    {
        //private IPropertyService _propertySvc;
        //private IRenterService _renterSvc;
        //private ITransactionService _transactionSvc;
        //private IRoomService _roomSvc;
        //private IResponseModel _response;

        //public CheckoutRenterCommandHandler(
        //    IPropertyService propertySvc,
        //    IRenterService renterSvc,
        //    ITransactionService transactionSvc,
        //    IRoomService roomSvc,
        //    IResponseModel response)
        //{
        //    _propertySvc = propertySvc;
        //    _renterSvc = renterSvc;
        //    _transactionSvc = transactionSvc;
        //    _roomSvc = roomSvc;
        //    _response = response;
        //}

        //public async Task<IResponseModel> Handle(CheckoutRenterCommand request, CancellationToken cancellationToken)
        //{
        //    PropertyInfoDTO propInfo = await _propertySvc.GetPropertyInfoAsync(request.PropId);
        //    RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(propInfo.Id, request.RoomId);
        //    RenterInfoDTO renterInfo = await _renterSvc.GetRenterInfoAsync(propInfo.Id, request.RenterId);

        //    if (propInfo != null && roomInfo != null && renterInfo != null)
        //    {
        //        await _renterSvc.DeleteRenterAsync(propInfo.Id, renterInfo.Id);
        //        await _roomSvc.UpdateBedCountAsync(propInfo.Id, roomInfo.RoomId, false);
        //        await _transactionSvc.UpdateRenterInvoicesStatusAsync(renterInfo.Id);

        //        _response.Status = true;
        //        _response.Message = "Checkout Success.";
        //        _response.StatusCode = HttpStatusCode.OK;
        //    }
        //    else
        //    {
        //        _response.Status = false;
        //        _response.Message = "Entity not found.";
        //        _response.StatusCode = HttpStatusCode.BadRequest;
        //    }

        //    return _response;
        //}
    }
}

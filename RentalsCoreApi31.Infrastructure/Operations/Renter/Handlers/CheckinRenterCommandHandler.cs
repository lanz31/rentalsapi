﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Services;
using RentalsCoreApi31.Infrastructure.Operations.Transactions.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Net;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Handlers
{
    public class CheckinRenterCommandHandler //: IRequestHandler<CheckinRenterCommand, IResponseModel>
    {
        //private ITransactionService _transactionSvc;
        //private IPropertyService _propertySvc;
        //private IRoomService _roomSvc;
        //private IRenterService _renterSvc;
        //private IResponseModel _response;

        //public CheckinRenterCommandHandler(
        //    ITransactionService transactionSvc,
        //    IPropertyService propertySvc,
        //    IRoomService roomSvc,
        //    IRenterService renterSvc,
        //    IResponseModel response)
        //{
        //    _transactionSvc = transactionSvc;
        //    _propertySvc = propertySvc;
        //    _roomSvc = roomSvc;
        //    _renterSvc = renterSvc;
        //    _response = response;
        //}

        //public async Task<IResponseModel> Handle(CheckinRenterCommand request, CancellationToken cancellationToken)
        //{
        //    PropertyInfoDTO propInfo = await _propertySvc.GetPropertyInfoAsync(request.PropId);
        //    RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(propInfo.Id, request.RoomId);
        //    RenterInfoDTO renterInfo = await _renterSvc.GetRenterInfoAsync(propInfo.Id, request.RenterId);

        //    if (propInfo != null && roomInfo != null && renterInfo != null)
        //    {
        //        if (roomInfo.OccupiedBeds < roomInfo.TotalBeds)
        //        {
        //            bool isCheckedIn = await _renterSvc.GetRenterCheckinStatusAsync(request.PropId, request.RenterId);

        //            if (!isCheckedIn)
        //            {
        //                CheckinModel checkin = new CheckinModel
        //                {
        //                    RenterId = request.RenterId,
        //                    RoomId = request.RoomId,
        //                    PropertyId = request.PropId
        //                };

        //                var checkinResult = await _transactionSvc.AddCheckInTransactionAsync(checkin);

        //                if (checkinResult.Status)
        //                {
        //                    RenterInfoDTO rentrInfo = await _renterSvc.GetRenterInfoAsync(propInfo.Id, request.RenterId);

        //                    InvoiceModel invoice = _transactionSvc.GenerateInvoice(roomInfo.Price, roomInfo.MonthsAdvance, roomInfo.MonthsDeposit, request.RenterId, Convert.ToDateTime(rentrInfo.CheckIn));
        //                    _response = await _transactionSvc.SaveInvoiceAsync(invoice);
        //                    _response.StatusCode = HttpStatusCode.OK;
        //                }
        //            }
        //            else
        //            {
        //                _response.Status = false;
        //                _response.Message = "Renter already checked in.";
        //                _response.StatusCode = HttpStatusCode.BadRequest;
        //            }
        //        }
        //        else
        //        {
        //            _response.Status = false;
        //            _response.Message = "Room fully occupied.";
        //            _response.StatusCode = HttpStatusCode.BadRequest;
        //        }
        //    }
        //    else
        //    {
        //        _response.Status = false;
        //        _response.Message = "Entity not found.";
        //        _response.StatusCode = HttpStatusCode.BadRequest;
        //    }

        //    return _response;
        //}
    }
}

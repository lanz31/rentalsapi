﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.DataContext;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using System.Linq;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Repository
{
    public interface IRenterRepository
    {
        //Task<IResponseModel> AddRenterAsync(Core.Models.Renter obj);
        //Task<RenterInfoDTO> GetRenterInfoAsync(long pid, long rid);
        //Task<IEnumerable<RenterListDTO>> GetRentersPerPropertyAsync(long pid);
        //Task<DateTime> GetRenterCheckinStatusAsync(long pid, long rid);
        //Task<IResponseModel> DeleteRenterAsync(long pid, long rid);
        //Task<decimal> GetRenterLatestInvoiceAsync(Guid rid);
        //Task<decimal> GetRenterBalanceAsync(Guid pid, Guid rid);
        //Task<DateTime> GetRenterLastBillingDueDateAsync(Guid rid);
    }

    public class RenterRepository : IRenterRepository
    {
        //private DatabaseContext _db;
        //private IResponseModel _response;
        //private ILoggerService _loggerService;
        //private IConfiguration _config;

        //public RenterRepository(
        //    DatabaseContext db,
        //    IResponseModel response,
        //    ILoggerService loggerService,
        //    IConfiguration config)
        //{
        //    _db = db;
        //    _response = response;
        //    _loggerService = loggerService;
        //    _config = config;
        //}

        //public async Task<IResponseModel> AddRenterAsync(Core.Models.Renter obj)
        //{
        //    try
        //    {
        //        _db.Renter.Add(obj);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Renter added successfully.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Add Renter", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding renter.";

        //        return _response;
        //    }
        //}

        //public async Task<RenterInfoDTO> GetRenterInfoAsync(long pid, long rid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var renterInfo = await con.QueryAsync<RenterInfoDTO>("sp_GetRenterInfo", new { PropId = pid, RenterId = rid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return renterInfo.Count() > 0 ? renterInfo.FirstOrDefault() : null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renter Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<IEnumerable<RenterListDTO>> GetRentersPerPropertyAsync(long pid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var renters = await con.QueryAsync<RenterListDTO>("sp_GetRenterList", new { PropId = pid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return renters.Count() > 0 ? renters : null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renters", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<DateTime> GetRenterCheckinStatusAsync(long pid, long rid)
        //    => await _db.Renter.Where(r => r.PropertyId == pid && r.Id == rid).Select(r => (DateTime) r.CheckIn).SingleOrDefaultAsync();

        //public async Task<IResponseModel> DeleteRenterAsync(long pid, long rid)
        //{
        //    try
        //    {
        //        Core.Models.Renter renter = await _db.Renter.Where(r => r.PropertyId == pid && r.Id == rid).SingleOrDefaultAsync();
        //        renter.Status = false;
        //        renter.CheckOut = DateTime.Now;

        //        _db.Update(renter);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Renter Deleted.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Delete/Checkout Renter", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error deleting/checkout renter.";

        //        return _response;
        //    }
        //}

        //public async Task<decimal> GetRenterLatestInvoiceAsync(Guid rid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var amountDue = await con.QueryAsync<decimal>("sp_GetLatestInvoiceDue", new { RenterId = rid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return Convert.ToDecimal(amountDue.SingleOrDefault());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renter Last Rent Due Amount", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return 0;
        //    }
        //}

        //public async Task<decimal> GetRenterBalanceAsync(Guid pid, Guid rid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var balance = await con.QueryAsync<decimal>("sp_GetRentBalance", new { PropId = pid, RenterId = rid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return Convert.ToDecimal(balance.SingleOrDefault());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renter Balance", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return 0;
        //    }
        //}

        //public async Task<DateTime> GetRenterLastBillingDueDateAsync(Guid rid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var billingDue = await con.QueryAsync<DateTime>("sp_GetRenterLastBillingDueDate", new { RenterId = rid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return billingDue.SingleOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renter Last Billing Due Date", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        throw ex;
        //    }
        //}
    }
}

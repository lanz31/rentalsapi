﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Repository;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Services
{
    public interface IRenterService
    {
        //Task<IResponseModel> AddRenterAsync(AddRenterModel model);
        //Task<RenterInfoDTO> GetRenterInfoAsync(long pid, long rid);
        //Task<IEnumerable<RenterListDTO>> GetRentersPerPropertyAsync(long pid);
        //Task<bool> GetRenterCheckinStatusAsync(long pid, long rid);
        //Task<IResponseModel> DeleteRenterAsync(long pid, long rid);
        //Task<decimal> GetLatestInvoiceAsync(Guid rid);
        //Task<decimal> GetRenterBalanceAsync(Guid pid, Guid rid);
        //Task<DateTime> GetRenterLastBillingDueDateAsync(Guid rid);
    }

    public class RenterService : IRenterService
    {
        //private IRenterRepository _repo;
        //private IResponseModel _response;

        //public RenterService(
        //    IRenterRepository repo,
        //    IResponseModel response)
        //{
        //    _repo = repo;
        //    _response = response;
        //}

        //public async Task<IResponseModel> AddRenterAsync(AddRenterModel model)
        //{
        //    var renter = new Core.Models.Renter
        //    {
        //        DateCreated = DateTime.Now,
        //        Name = model.Name,
        //        Email = model.Email,
        //        Address = model.Address,
        //        ContactNo = model.ContactNo,
        //        Profession = model.Profession,
        //        PropertyId = model.PropertyId,
        //        Status = true
        //    };

        //    return await _repo.AddRenterAsync(renter);
        //}

        //public async Task<RenterInfoDTO> GetRenterInfoAsync(long pid, long rid) => await _repo.GetRenterInfoAsync(pid, rid);

        //public async Task<IEnumerable<RenterListDTO>> GetRentersPerPropertyAsync(long pid) => await _repo.GetRentersPerPropertyAsync(pid);

        // TODO: Refresh room drop down menu in frontend checkin component to get updated available rooms
        //public async Task<bool> GetRenterCheckinStatusAsync(long pid, long rid)
        //{
        //    DateTime checkinDate = await _repo.GetRenterCheckinStatusAsync(pid, rid);

        //    return checkinDate.ToShortDateString() != "01/01/0001" ? true : false;
        //}

        //public async Task<IResponseModel> DeleteRenterAsync(long pid, long rid) => await _repo.DeleteRenterAsync(pid, rid);

        //public async Task<decimal> GetLatestInvoiceAsync(Guid rid) => await _repo.GetRenterLatestInvoiceAsync(rid);

        //public async Task<decimal> GetRenterBalanceAsync(Guid pid, Guid rid) => await _repo.GetRenterBalanceAsync(pid, rid);

        //public async Task<DateTime> GetRenterLastBillingDueDateAsync(Guid rid) => await _repo.GetRenterLastBillingDueDateAsync(rid);
    }
}
﻿using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Validator
{
    public class AddRenterCommandValidator : AbstractValidator<AddRenterCommand>
    {
        public AddRenterCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Name field is mandatory.");

            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .WithMessage("Email field is mandatory.")
                .EmailAddress()
                .WithMessage("Not valid email.");

            RuleFor(x => x.Address)
                .NotEmpty()
                .NotNull()
                .WithMessage("Address field is mandatory.");

            RuleFor(x => x.ContactNo)
                .MinimumLength(11)
                .NotEmpty()
                .NotNull()
                .WithMessage("Contact no field is mandatory.");

            RuleFor(x => x.Profession)
                .NotEmpty()
                .NotNull()
                .WithMessage("Profession field is mandatory");

            RuleFor(x => x.PropertyId)
                .NotNull()
                .WithMessage("Property id field is mandatory.");
        }
    }
}

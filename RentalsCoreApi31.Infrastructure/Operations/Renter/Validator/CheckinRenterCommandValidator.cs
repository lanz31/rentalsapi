﻿using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Validator
{
    public class CheckinRenterCommandValidator : AbstractValidator<CheckinRenterCommand>
    {
        public CheckinRenterCommandValidator()
        {
            RuleFor(x => x.PropId)
                .NotEmpty()
                .WithMessage("Property id required.");

            RuleFor(x => x.RoomId)
                .NotEmpty()
                .WithMessage("Room id required.");

            RuleFor(x => x.RenterId)
                .NotEmpty()
                .WithMessage("Renter id required.");
        }
    }
}

﻿using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Renter.Validator
{
    public class CheckoutRenterCommandValidator : AbstractValidator<CheckoutRenterCommand>
    {
        public CheckoutRenterCommandValidator()
        {
            RuleFor(r => r.PropId)
                .NotEmpty()
                .WithMessage("Property id required.")
                .GreaterThan(0)
                .WithMessage("Invalid property id.");

            RuleFor(r => r.RoomId)
                .NotEmpty()
                .WithMessage("Room id required.")
                .GreaterThan(0)
                .WithMessage("Invalid room id.");

            RuleFor(r => r.RenterId)
                .NotEmpty()
                .WithMessage("Renter id required.")
                .GreaterThan(0)
                .WithMessage("Invalid renter id."); ;
        }
    }
}

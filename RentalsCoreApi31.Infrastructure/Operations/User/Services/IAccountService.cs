﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.JWT;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Core.Services;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;

namespace RentalsCoreApi31.Infrastructure.Operations.User.Services
{
    public interface IAccountService
    {
        Task<IResponseModel<Response>> RegisterUserAsync(UserRegisterModel model);
        //Task<IResponseModel> VerifyEmailAsync(string email);
        Task<TokenModel> VerifyUserAsync(UserLoginModel obj);
    }

    public class AccountService : IAccountService
    {
        private IResponseService<Response> _responseSvc;
        private IConfiguration _config;
        private UserManager<ApplicationUser> _user;
        private RoleManager<ApplicationRole> _role;

        public AccountService(
            IResponseService<Response> responseSvc,
            IConfiguration config,
            UserManager<ApplicationUser> user,
            RoleManager<ApplicationRole> role)
        {
            _responseSvc = responseSvc;
            _config = config;
            _user = user;
            _role = role;
        }

        public async Task<IResponseModel<Response>> RegisterUserAsync(UserRegisterModel model)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = model.Email,
                Name = model.Name,
                Email = model.Email
            };

            bool roleExists = await _role.RoleExistsAsync(model.Role);

            if (!roleExists) return _responseSvc.Response(false, "User registration failed.", HttpStatusCode.BadRequest);

            IdentityResult result = await _user.CreateAsync(user, model.Password);

            if (!result.Succeeded) return _responseSvc.Response(false, "User registration failed.", HttpStatusCode.BadRequest);

            await _user.AddToRoleAsync(user, model.Role);
            return _responseSvc.Response(true, "User registration successful.", HttpStatusCode.OK);
        }

        //public async Task<IResponseModel> VerifyEmailAsync(string email)
        //{
        //    var user = await _repo.GetUserInfoAsync(email);

        //    if (user != null)
        //    {
        //        _response.Status = false;
        //        _response.Message = "Email already exists.";
        //    }
        //    else
        //    {
        //        _response.Status = true;
        //        _response.Message = "Email available.";
        //    }

        //    return _response;
        //}

        public async Task<TokenModel> VerifyUserAsync(UserLoginModel obj)
        {
            ApplicationUser user = await _user.FindByEmailAsync(obj.Email);

            if (user != null)
            {
                bool isAuthenticated = await _user.CheckPasswordAsync(user, obj.Password);

                if (isAuthenticated)
                {
                    var token = new JwtTokenBuilder(_config);

                    var roles = await _user.GetRolesAsync(user);
                    List<Claim> claims = new List<Claim>();
                    claims.Add(new Claim(ClaimTypes.Role, roles.FirstOrDefault()));

                    return new TokenModel
                    {
                        AccessToken = token.GenerateToken(claims),
                        UserId = user.Id,
                        Email = user.Email
                    };
                }
            }

            return new TokenModel
            {
                AccessToken = "",
                UserId = 0,
                Email = ""
            };
        }
    }
}
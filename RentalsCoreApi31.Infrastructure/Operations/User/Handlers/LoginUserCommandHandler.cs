﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.User.Commands;
using RentalsCoreApi31.Infrastructure.Operations.User.Services;

using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace RentalsCoreApi31.Infrastructure.Operations.User.Handlers
{
    public class LoginUserCommandHandler : IRequestHandler<LoginUserCommand, TokenModel>
    {
        private IAccountService _accountSvc;

        public LoginUserCommandHandler(IAccountService accountSvc)
        {
            _accountSvc = accountSvc;
        }

        public async Task<TokenModel> Handle(LoginUserCommand request, CancellationToken cancellationToken)
        {
            UserLoginModel user = new UserLoginModel
            {
                Email = request.Email,
                Password = request.Password
            };

            return await _accountSvc.VerifyUserAsync(user);
        }
    }
}

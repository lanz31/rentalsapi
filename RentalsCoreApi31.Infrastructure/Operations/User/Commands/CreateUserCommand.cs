﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;

namespace RentalsCoreApi31.Infrastructure.Operations.User.Commands
{
    public class CreateUserCommand //: IRequest<IResponseModel>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}

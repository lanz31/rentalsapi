﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;

namespace RentalsCoreApi31.Infrastructure.Operations.User.Commands
{
    public class LoginUserCommand : IRequest<TokenModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}

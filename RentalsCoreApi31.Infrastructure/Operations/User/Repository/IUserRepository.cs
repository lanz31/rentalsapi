﻿using RentalsCoreApi31.Infrastructure.DataContext;
using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Core.Models;

using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RentalsCoreApi31.Infrastructure.Operations.User.Repository
{
    public interface IUserRepository
    {
        //Task<IResponseModel> SaveUserAsync(ApplicationUser obj);
        //Task<ApplicationUser> GetUserInfoAsync(string email);
    }

    public class UserRepository : IUserRepository
    {
        //private DatabaseContext _db;
        //private IResponseModel _response;
        //private ILoggerService _loggerService;

        //public UserRepository(
        //    DatabaseContext db,
        //    IResponseModel response,
        //    ILoggerService loggerService)
        //{
        //    _db = db;
        //    _response = response;
        //    _loggerService = loggerService;
        //}

        //public async Task<IResponseModel> SaveUserAsync(ApplicationUser obj)
        //{
        //    try
        //    {
        //        await _db.Users.AddAsync(obj);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Registration Successful.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Register User", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Registration Error.";

        //        return _response;
        //    }
        //}

        //public async Task<ApplicationUser> GetUserInfoAsync(string email)
        //{
        //    try
        //    {
        //        return await _db.Users.Where(u => u.Email == email).SingleOrDefaultAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get User Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Get User Info Error.";

        //        return null;
        //    }
        //}
    }
}

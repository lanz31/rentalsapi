﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.DataContext;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using System.Data;

namespace RentalsCoreApi31.Infrastructure.Operations.Transactions.Repository
{
    public interface ITransactionsRepository
    {
        //Task<IResponseModel> UpdateRenterCheckInDateAsync(Core.Models.Renter obj);
        //Task<IResponseModel> AddInvoiceAsync(Invoice obj);
        //Task<IResponseModel> AddTransactionAsync(TransactionHistory obj);
        //Task<List<Invoice>> GetRenterInvoicesAsync(long rid);
        //Task<IResponseModel> DeleteInvoiceAsync(List<Invoice> invoices);
    }

    public class TransactionsRepository : ITransactionsRepository
    {
        //private DatabaseContext _db;
        //private IResponseModel _response;
        //private ILoggerService _loggerService;
        //private IConfiguration _config;

        //public TransactionsRepository(
        //    DatabaseContext db,
        //    IResponseModel response,
        //    ILoggerService loggerService,
        //    IConfiguration config)
        //{
        //    _db = db;
        //    _response = response;
        //    _loggerService = loggerService;
        //    _config = config;
        //}

        //public async Task<IResponseModel> UpdateRenterCheckInDateAsync(Core.Models.Renter obj)
        //{
        //    try
        //    {
        //        _db.Renter.Update(new Core.Models.Renter
        //        {
        //            Id = obj.Id,
        //            PropertyId = obj.PropertyId,
        //            CheckIn = obj.CheckIn,
        //            RoomId = obj.RoomId,
        //            Name = obj.Name,
        //            Address = obj.Address,
        //            Profession = obj.Profession,
        //            ContactNo = obj.ContactNo,
        //            Email = obj.Email,
        //            Status = obj.Status
        //        });
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Renter Checkin successfully.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Renter Checkin", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding renter checkin.";

        //        return _response;
        //    }
        //}

        //public async Task<IResponseModel> AddInvoiceAsync(Invoice obj)
        //{
        //    try
        //    {
        //        _db.Invoices.Add(obj);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Invoice added successfully.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Add Invoice", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding invoice.";

        //        return _response;
        //    }
        //}

        //public async Task<IResponseModel> AddTransactionAsync(TransactionHistory obj)
        //{
        //    try
        //    {
        //        _db.Transactions.Add(obj);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Transaction added successfully.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Add Transaction", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error adding transaction.";

        //        return _response;
        //    }
        //}

        //public async Task<List<Invoice>> GetRenterInvoicesAsync(long rid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var invoices = await con.QueryAsync<Invoice>("sp_GetRenterInvoicesStatus", new { RenterId = rid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return invoices.Count() > 0 ? invoices.ToList() : null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Get Renter's Invoices", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}

        //public async Task<IResponseModel> DeleteInvoiceAsync(List<Invoice> invoices)
        //{
        //    try
        //    {
        //        _db.UpdateRange(invoices);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Invoices Deleted.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Delete Invoices", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Error deleting invoices.";

        //        return _response;
        //    }
        //}
    }
}

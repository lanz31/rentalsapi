﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Infrastructure.Operations.Transactions.Repository;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RentalsCoreApi31.Infrastructure.Operations.Transactions.Services
{
    public interface ITransactionService
    {
        //Task<IResponseModel> AddCheckInTransactionAsync(CheckinModel model);
        //InvoiceModel GenerateInvoice(decimal roomPrice, decimal monthsAdv, decimal monthsDep, long rid, DateTime checkin);
        //InvoiceModel GenerateInvoice(decimal amountDue, long rid, DateTime billingDue);
        //Task<IResponseModel> SaveInvoiceAsync(InvoiceModel model);
        //Task<IResponseModel> UpdateRenterInvoicesStatusAsync(long rid);
        //Task<IResponseModel> SaveTransactionAsync(AddTransactionHistoryModel model);
        //Task<List<PropertyTransactionsDTO>> GetTransactionPerPropertyAsync(BaseModel model);
        //Task<IResponseModel> BillsPaymentAsync(decimal payment, Guid renterid);
    }

    public class TransactionService : ITransactionService
    {
        //private ITransactionsRepository _transactionRepo;
        //private IRenterService _renterSvc;
        //private IRoomService _roomSvc;
        //private IResponseModel _response;

        //public TransactionService(
        //    ITransactionsRepository transactionRepo,
        //    IRenterService renterSvc,
        //    IRoomService roomSvc,
        //    IResponseModel response)
        //{
        //    _transactionRepo = transactionRepo;
        //    _renterSvc = renterSvc;
        //    _roomSvc = roomSvc;
        //    _response = response;
        //}

        //public async Task<IResponseModel> AddCheckInTransactionAsync(CheckinModel model)
        //{
        //    var renterInfo = await _renterSvc.GetRenterInfoAsync(model.PropertyId, model.RenterId);

        //    var renter = new Core.Models.Renter
        //    {
        //        Id = renterInfo.Id,
        //        CheckIn = DateTime.Now,
        //        RoomId = model.RoomId,
        //        PropertyId = renterInfo.PropertyId,
        //        Name = renterInfo.Name,
        //        Address = renterInfo.Address,
        //        Profession = renterInfo.Profession,
        //        ContactNo = renterInfo.ContactNo,
        //        Email = renterInfo.Email,
        //        Status = true,
        //        DateCreated = Convert.ToDateTime(renterInfo.DateCreated)
        //    };

        //    var result = await _transactionRepo.UpdateRenterCheckInDateAsync(renter);

        //    if (result.Status) _response = await _roomSvc.UpdateBedCountAsync(model.PropertyId, model.RoomId, true);

        //    return _response;
        //}

        //public InvoiceModel GenerateInvoice(decimal roomPrice, decimal monthsAdv, decimal monthsDep, long rid, DateTime checkin)
        //{
        //    return new InvoiceModel
        //    {
        //        AmountDue = roomPrice * (monthsAdv + monthsDep),
        //        BillingDue = checkin.AddMonths(1),
        //        RenterId = rid
        //    };
        //}

        //public InvoiceModel GenerateInvoice(decimal amountDue, long rid, DateTime billingDue)
        //{
        //    return new InvoiceModel
        //    {
        //        AmountDue = amountDue,
        //        BillingDue = billingDue.AddMonths(1),
        //        RenterId = rid
        //    };
        //}

        //public async Task<IResponseModel> SaveInvoiceAsync(InvoiceModel model)
        //{
        //    return await _transactionRepo.AddInvoiceAsync(new Invoice
        //    {
        //        DateCreated = DateTime.Now,
        //        AmountDue = model.AmountDue,
        //        BillingDue = model.BillingDue,
        //        RenterId = model.RenterId,
        //        Status = true
        //    });
        //}

        //public async Task<IResponseModel> UpdateRenterInvoicesStatusAsync(long rid)
        //{
        //    List<Invoice> invoices = await _transactionRepo.GetRenterInvoicesAsync(rid);

        //    foreach (var invoice in invoices) invoice.Status = false;

        //    return await _transactionRepo.DeleteInvoiceAsync(invoices);
        //}

        //public async Task<IResponseModel> SaveTransactionAsync(AddTransactionHistoryModel model)
        //{
        //    return await _transactionRepo.AddTransactionAsync(new TransactionHistory
        //    {
        //        Id = Guid.NewGuid(),
        //        DateCreated = DateTime.Now,
        //        AmountDue = model.AmountDue,
        //        AmountPaid = model.AmountPaid,
        //        PaymentFor = model.PaymentFor,
        //        Balance = model.Balance,
        //        PropertyId = model.PropertyId,
        //        RenterId = model.RenterId
        //    });
        //}
    }
}
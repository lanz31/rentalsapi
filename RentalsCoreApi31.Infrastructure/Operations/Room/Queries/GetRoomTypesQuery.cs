﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System.Collections.Generic;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Queries
{
    public class GetRoomTypesQuery : IRequest<IResponseModel<IEnumerable<RoomTypesDTO>>>
    {
        public long PropId { get; set; }

        public GetRoomTypesQuery(long propid)
        {
            PropId = propid;
        }
    }
}

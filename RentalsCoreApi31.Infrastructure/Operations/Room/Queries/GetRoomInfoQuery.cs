﻿using RentalsCoreApi31.Core.BusinessModels.DTO;

using MediatR;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Queries
{
    public class GetRoomInfoQuery : IRequest<RoomInfoDTO>
    {
        public long PropId { get; set; }
        public long RoomId { get; set; }

        public GetRoomInfoQuery(long propid, long roomid)
        {
            PropId = propid;
            RoomId = roomid;
        }
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels.DTO;

using MediatR;
using System.Collections.Generic;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Queries
{
    public class GetRoomsQuery : IRequest<IEnumerable<RoomsListInfoDTO>>
    {
        public long PropId { get; set; }
        public bool Filter { get; set; }

        public GetRoomsQuery(long propid, bool filter)
        {
            PropId = propid;
            Filter = filter;
        }
    }
}

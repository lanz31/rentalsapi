﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.DataContext;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using System.Linq;
using Dapper;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Repository
{
    public interface IRoomRepository
    {
        #region Room Types
        Task<RoomTypes> AddRoomTypesAsync(RoomTypes obj);
        Task<RoomTypesDTO> GetRoomTypeAsync(long pid, string name);
        Task<RoomTypesDTO> GetRoomTypeAsync(long pid, long roomtypeid);
        Task<IEnumerable<RoomTypesDTO>> GetRoomTypesPerPropertyAsync(long pid);
        Task<RoomTypes> UpdateRoomTypeAsync(RoomTypes obj);
        #endregion

        #region Room
        Task<Core.Models.Room> AddRoomAsync(Core.Models.Room obj);
        Task<IEnumerable<RoomsListInfoDTO>> GetPropertyRoomsAsync(long pid, bool filter = false);
        Task<RoomInfoDTO> GetRoomInfoByNameAsync(long pid, string roomname);
        Task<RoomInfoDTO> GetRoomInfoAsync(long pid, long rid);
        Task<bool> UpdateBedCountAsync(long pid, long rid, int count);
        #endregion

        //Task<AvailableBedsDTO> GetAvailableBedsPerRoomAsync(Guid userid, Guid propertyid, Guid roomid);

        #region Room Features
        Task<RoomFeatures> AddRoomFeatureAsync(RoomFeatures obj);
        Task<string> GetRoomFeatureAsync(long rid, string name);
        Task<IEnumerable<string>> GetRoomFeaturesAsync(long rid);
        #endregion
    }

    public class RoomRepository : IRoomRepository
    {
        private DatabaseContext _db;
        private ILoggerService _loggerService;
        private IConfiguration _config;

        public RoomRepository(
            DatabaseContext db,
            ILoggerService loggerService,
            IConfiguration config)
        {
            _db = db;
            _loggerService = loggerService;
            _config = config;
        }

        #region Room Types
        public async Task<RoomTypes> AddRoomTypesAsync(RoomTypes obj)
        {
            try
            {
                _db.RoomTypes.Add(obj);
                await _db.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Add Property Room Type", ex.InnerException.Message, ex.Message, ex.StackTrace);
                return null;
            }
        }

        public async Task<RoomTypesDTO> GetRoomTypeAsync(long pid, string name)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var roomType = await con.QueryAsync<RoomTypesDTO>("sp_GetRoomTypeName", new { PropId = pid, RoomTypeName = name }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return roomType.Count() > 0 ? roomType.FirstOrDefault() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Room Type Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<RoomTypesDTO> GetRoomTypeAsync(long pid, long roomtypeid)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var roomType = await con.QueryAsync<RoomTypesDTO>("sp_GetRoomTypeById", new { PropId = pid, RoomTypeId = roomtypeid }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return roomType.Count() > 0 ? roomType.FirstOrDefault() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Room Type Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<IEnumerable<RoomTypesDTO>> GetRoomTypesPerPropertyAsync(long pid)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var roomTypes = await con.QueryAsync<RoomTypesDTO>("sp_GetPropertyRoomTypes", new { PropId = pid }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return roomTypes.Count() > 0 ? roomTypes.ToList() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Property Room Types", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<RoomTypes> UpdateRoomTypeAsync(RoomTypes obj)
        {
            try
            {
                _db.RoomTypes.Update(obj);
                await _db.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Room Type Updated", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }
        #endregion

        #region Room
        public async Task<Core.Models.Room> AddRoomAsync(Core.Models.Room obj)
        {
            try
            {
                _db.Rooms.Add(obj);
                await _db.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Add Room", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<IEnumerable<RoomsListInfoDTO>> GetPropertyRoomsAsync(long pid, bool filter = false)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var rooms = await con.QueryAsync<RoomsListInfoDTO>("sp_GetPropertyRooms", new { PropId = pid, Filter = filter }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return rooms.ToList() ?? null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Property Room Types", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<RoomInfoDTO> GetRoomInfoByNameAsync(long pid, string roomname)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var room = await con.QueryAsync<RoomInfoDTO>("sp_GetRoomByName", new { PropId = pid, RoomName = roomname }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return room.Count() > 0 ? room.FirstOrDefault() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Property Room Types", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<RoomInfoDTO> GetRoomInfoAsync(long pid, long rid)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var roomInfo = await con.QueryAsync<RoomInfoDTO>("sp_GetRoomInfo", new { PropId = pid, RoomId = rid }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return roomInfo.Count() > 0 ? roomInfo.Single() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Room Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<bool> UpdateBedCountAsync(long pid, long rid, int count)
        {
            try
            {
                var room = await _db.Rooms.Where(r => r.PropertyId == pid && r.Id == rid).SingleOrDefaultAsync();
                room.OccupiedBeds = count;
                room.DateCreated = room.DateCreated;

                _db.Rooms.Update(room);
                await _db.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Update Bed Count", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return false;
            }
        }
        #endregion

        #region Room Features
        public async Task<RoomFeatures> AddRoomFeatureAsync(RoomFeatures obj)
        {
            try
            {
                _db.RoomFeatures.Add(obj);
                await _db.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                _loggerService.Log("Add Room Feature", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<string> GetRoomFeatureAsync(long rid, string name)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var roomFeature = await con.QueryAsync<string>("sp_GetRoomFeature", new { RoomId = rid, Name = name }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return roomFeature.Count() > 0 ? roomFeature.FirstOrDefault() : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Room Type Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }

        public async Task<IEnumerable<string>> GetRoomFeaturesAsync(long rid)
        {
            try
            {
                using (var con = new SqlConnection(_config["Database:ConnectionString"]))
                {
                    con.Open();

                    var roomFeatures = await con.QueryAsync<string>("sp_GetRoomFeatures", new { RoomId = rid }, commandType: CommandType.StoredProcedure);

                    con.Close();

                    return roomFeatures.Count() > 0 ? roomFeatures : null;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Log("Get Room Features", ex.InnerException.Message, ex.Message, ex.StackTrace);

                return null;
            }
        }
        #endregion
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Commands
{
    public class AddRoomFeatureCommand //: IRequest<IResponseModel>
    {
        public string Name { get; set; }
        public long RoomId { get; set; }
    }
}

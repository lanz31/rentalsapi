﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;
using System;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Commands
{
    public class UpdateRoomTypeCommand //: IRequest<IResponseModel>
    {
        public long RoomTypeId { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public decimal MonthsAdvance { get; set; }
        public decimal MonthsDeposit { get; set; }
        public long PropertyId { get; set; }
    }
}

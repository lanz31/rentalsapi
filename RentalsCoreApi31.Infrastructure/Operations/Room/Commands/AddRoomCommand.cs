﻿using RentalsCoreApi31.Core.BusinessModels;

using MediatR;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Commands
{
    public class AddRoomCommand //: IRequest<IResponseModel<Response>>
    {
        public string Name { get; set; }
        public int TotalBeds { get; set; }
        public long RoomTypeId { get; set; }
        public long PropertyId { get; set; }
    }
}

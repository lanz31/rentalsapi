﻿using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Validator
{
    public class AddRoomTypeCommandValidator : AbstractValidator<AddRoomTypeCommand>
    {
        public AddRoomTypeCommandValidator()
        {
            RuleFor(x => x.Type)
                .NotEmpty()
                .NotNull()
                .WithMessage("Type field is mandatory.");
            
            RuleFor(x => x.Price)
                .GreaterThan(0)
                .WithMessage("Price field should be greater than 0")
                .NotEmpty()
                .NotNull()
                .WithMessage("Price field is mandatory.");
            
            RuleFor(x => x.MonthsAdvance)
                .GreaterThanOrEqualTo(1)
                .WithMessage("Month advance must at least 1 month or so")
                .NotEmpty()
                .NotNull()
                .WithMessage("Month advance field is mandatory.");
            
            RuleFor(x => x.MonthsDeposit)
                .GreaterThanOrEqualTo(1)
                .WithMessage("Month deposit must at least 1 month or so")
                .NotEmpty()
                .NotNull()
                .WithMessage("Month deposit field is mandatory.");
            
            RuleFor(x => x.PropertyId)
                .NotEmpty()
                .NotNull()
                .WithMessage("Property id field is mandatory.");
        }
    }
}

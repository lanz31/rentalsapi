﻿using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Validator
{
    public class AddRoomFeatureCommandValidator : AbstractValidator<AddRoomFeatureCommand>
    {
        public AddRoomFeatureCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Feature name field mandatory.");

            RuleFor(x => x.RoomId)
                .NotNull()
                .WithMessage("Room id field mandatory.");
        }
    }
}

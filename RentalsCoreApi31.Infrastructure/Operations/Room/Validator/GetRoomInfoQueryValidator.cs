﻿using RentalsCoreApi31.Infrastructure.Operations.Room.Queries;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Validator
{
    public class GetRoomInfoQueryValidator : AbstractValidator<GetRoomInfoQuery>
    {
        public GetRoomInfoQueryValidator()
        {
            RuleFor(x => x.PropId).NotEmpty().NotNull().WithMessage("Property id field is mandatory.");
            RuleFor(x => x.RoomId).NotEmpty().NotNull().WithMessage("Room id field is mandatory.");
        }
    }
}

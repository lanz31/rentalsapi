﻿using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Validator
{
    public class AddRoomCommandValidator : AbstractValidator<AddRoomCommand>
    {
        public AddRoomCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Name field is mandatory.");

            RuleFor(x => x.TotalBeds)
                .GreaterThanOrEqualTo(1)
                .WithMessage("Bed count must be more than 1.");

            RuleFor(x => x.RoomTypeId)
                .NotEmpty()
                .NotNull()
                .WithMessage("Room type field is mandatory.");

            RuleFor(x => x.PropertyId)
                .NotEmpty()
                .NotNull()
                .WithMessage("Property id field is mandatory.");
        }
    }
}

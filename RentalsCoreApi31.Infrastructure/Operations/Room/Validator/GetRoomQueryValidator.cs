﻿using RentalsCoreApi31.Infrastructure.Operations.Room.Queries;

using FluentValidation;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Validator
{
    public class GetRoomQueryValidator : AbstractValidator<GetRoomsQuery>
    {
        public GetRoomQueryValidator()
        {
            RuleFor(x => x.PropId).NotEmpty().NotNull().WithMessage("Property id field is mandatory.");
        }
    }
}

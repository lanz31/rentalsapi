﻿using FluentValidation;
using RentalsCoreApi31.Infrastructure.Operations.Room.Queries;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Validator
{
    public class GetRoomTypesQueryValidator : AbstractValidator<GetRoomTypesQuery>
    {
        public GetRoomTypesQueryValidator()
        {
            RuleFor(x => x.PropId).NotEmpty().NotNull().WithMessage("Property id field is mandatory.");
        }
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using RentalsCoreApi31.Core.Services;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Handlers
{
    public class AddRoomCommandHandler //: IRequestHandler<AddRoomCommand, IResponseModel<Response>>
    {
        //private IRoomService _roomSvc;
        //private IResponseService<Response> _responseSvc;

        //public AddRoomCommandHandler(IRoomService roomSvc, IResponseService<Response> responseSvc)
        //{
        //    _roomSvc = roomSvc;
        //    _responseSvc = responseSvc;
        //}

        //public async Task<IResponseModel<Response>> Handle(AddRoomCommand request, CancellationToken cancellationToken)
        //{
        //    var isRoomExists = await _roomSvc.VerifyRoomAsync(request.PropertyId, request.Name);

        //    if (!isRoomExists.Status)
        //    {
        //        var isRoomTypeExist = await _roomSvc.VerifyRoomTypeAsync(request.PropertyId, request.RoomTypeId);

        //        if (isRoomTypeExist.Status)
        //        {
        //            AddRoomModel room = new AddRoomModel
        //            {
        //                Name = request.Name,
        //                TotalBeds = request.TotalBeds,
        //                RoomTypeId = request.RoomTypeId,
        //                PropertyId = request.PropertyId
        //            };

        //            _response = await _roomSvc.AddRoomAsync(room);
        //            _response.StatusCode = HttpStatusCode.OK;
        //        }
        //        else
        //        {
        //            _response.Status = false;
        //            _response.Message = isRoomTypeExist.Message;
        //            _response.StatusCode = HttpStatusCode.BadRequest;
        //        }
        //    }
        //    else
        //    {
        //        _response.Status = false;
        //        _response.Message = "Invalid room details.";
        //        _response.StatusCode = HttpStatusCode.BadRequest;
        //    }

        //    return _response;
        //}
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;

using MediatR;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Handlers
{
    public class AddRoomFeatureCommandHandler //: IRequestHandler<AddRoomFeatureCommand, IResponseModel>
    {
        //private IRoomService _roomSvc;
        //private IResponseModel _response;

        //public AddRoomFeatureCommandHandler(IRoomService roomSvc, IResponseModel response)
        //{
        //    _roomSvc = roomSvc;
        //    _response = response;
        //}

        //public async Task<IResponseModel> Handle(AddRoomFeatureCommand request, CancellationToken cancellationToken)
        //{
        //    RoomInfoDTO roomInfo = await _roomSvc.GetRoomInfoAsync(0, request.RoomId);
        //    var isExisting = await _roomSvc.VerifyRoomFeatureAsync(request.RoomId, request.Name);

        //    if (roomInfo != null)
        //    {
        //        if (!isExisting.Status)
        //        {
        //            AddRoomFeaturesModel roomFeature = new AddRoomFeaturesModel
        //            {
        //                Name = request.Name,
        //                RoomId = request.RoomId
        //            };

        //            _response = await _roomSvc.AddRoomFeatureAsync(roomFeature);
        //            _response.StatusCode = HttpStatusCode.OK;
        //        }
        //        else
        //        {
        //            _response.Status = false;
        //            _response.Message = "Feature already exist.";
        //            _response.StatusCode = HttpStatusCode.BadRequest;
        //        }
        //    }
        //    else
        //    {
        //        _response.Status = false;
        //        _response.Message = "Room does not exist.";
        //        _response.StatusCode = HttpStatusCode.BadRequest;
        //    }

        //    return _response;
        //}
    }
}

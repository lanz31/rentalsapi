﻿using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.Room.Queries;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;

using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Handlers
{
    public class GetRoomTypesQueryHandler : IRequestHandler<GetRoomTypesQuery, IResponseModel<IEnumerable<RoomTypesDTO>>>
    {
        private IRoomService _roomSvc;

        public GetRoomTypesQueryHandler(IRoomService roomSvc)
        {
            _roomSvc = roomSvc;
        }

        public async Task<IResponseModel<IEnumerable<RoomTypesDTO>>> Handle(GetRoomTypesQuery request, CancellationToken cancellationToken)
            => await _roomSvc.GetRoomTypesPerPropertyAsync(request.PropId) ?? null;
    }
}

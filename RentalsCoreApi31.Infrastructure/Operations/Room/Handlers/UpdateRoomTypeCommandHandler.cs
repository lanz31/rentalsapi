﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Handlers
{
    public class UpdateRoomTypeCommandHandler //: IRequestHandler<UpdateRoomTypeCommand, IResponseModel>
    {
        //private IRoomService _roomSvc;
        //private IPropertyService _propertySvc;
        //private IResponseService _responseSvc;

        //public UpdateRoomTypeCommandHandler(
        //    IRoomService roomSvc,
        //    IPropertyService propertySvc,
        //    IResponseService responseSvc)
        //{
        //    _roomSvc = roomSvc;
        //    _propertySvc = propertySvc;
        //    _responseSvc = responseSvc;
        //}

        //public async Task<IResponseModel> Handle(UpdateRoomTypeCommand request, CancellationToken cancellationToken)
        //{
        //    PropertyInfoDTO property = await _propertySvc.GetPropertyInfoAsync(request.PropertyId);
        //    if (property == null) return _responseSvc.Response(false, "Property not found.", HttpStatusCode.BadRequest);

        //    var roomTypeExist = await _roomSvc.VerifyRoomTypeAsync(property.Id, request.RoomTypeId);
        //    if (!roomTypeExist.Status) return _responseSvc.Response(false, "Room type not found.", HttpStatusCode.BadRequest);

        //    UpdateRoomTypeModel roomType = new UpdateRoomTypeModel
        //    {
        //        RoomTypeId = request.RoomTypeId,
        //        Type = request.Type,
        //        Price = request.Price,
        //        MonthsAdvance = request.MonthsAdvance,
        //        MonthsDeposit = request.MonthsDeposit,
        //        PropertyId = request.PropertyId
        //    };

        //    return await _roomSvc.UpdateRoomTypeAsync(roomType);
        //}
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;

using MediatR;
using System.Threading.Tasks;
using System.Threading;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Handlers
{
    public class AddRoomTypeCommandHandler : IRequestHandler<AddRoomTypeCommand, IResponseModel<Response>>
    {
        private IRoomService _roomSvc;
        private IPropertyService _propertySvc;
        private IResponseService<Response> _responseSvc;

        public AddRoomTypeCommandHandler(
            IRoomService roomSvc,
            IPropertyService propertySvc,
            IResponseService<Response> responseSvc)
        {
            _roomSvc = roomSvc;
            _propertySvc = propertySvc;
            _responseSvc = responseSvc;
        }

        public async Task<IResponseModel<Response>> Handle(AddRoomTypeCommand request, CancellationToken cancellationToken)
        {
            IResponseModel<PropertyInfoDTO> property = await _propertySvc.GetPropertyInfoAsync(request.PropertyId);

            if (property.Data == null) return _responseSvc.Response(property.Status, property.Message, property.StatusCode);

            var isExisting = await _roomSvc.VerifyRoomTypeAsync(property.Data.Id, request.Type);

            if (!isExisting.Status) return _responseSvc.Response(isExisting.Status, isExisting.Message, isExisting.StatusCode);

            AddRoomTypeModel roomType = new AddRoomTypeModel
            {
                Type = request.Type,
                Price = request.Price,
                MonthsAdvance = request.MonthsAdvance,
                MonthsDeposit = request.MonthsDeposit,
                PropertyId = request.PropertyId
            };

            return await _roomSvc.AddRoomTypesAsync(roomType);
        }
    }
}

﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Core.Models;
using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RentalsCoreApi31.Infrastructure.Operations.Room.Services
{
    public interface IRoomService
    {
        #region Room Types
        Task<IResponseModel<Response>> AddRoomTypesAsync(AddRoomTypeModel model);
        Task<IResponseModel<Response>> VerifyRoomTypeAsync(long pid, string roomType);
        //Task<IResponseModel> VerifyRoomTypeAsync(long pid, long roomTypeId);
        Task<IResponseModel<IEnumerable<RoomTypesDTO>>> GetRoomTypesPerPropertyAsync(long pid);
        //Task<IResponseModel> UpdateRoomTypeAsync(UpdateRoomTypeModel obj);
        #endregion

        #region Rooms
        //Task<IResponseModel> AddRoomAsync(AddRoomModel model);
        //Task<IResponseModel> VerifyRoomAsync(long pid, string roomname);
        //Task<IEnumerable<RoomsListInfoDTO>> GetPropertyRoomsAsync(long pid, bool filter = false);
        //Task<RoomInfoDTO> GetRoomInfoAsync(long pid, long rid);
        //Task<IResponseModel> UpdateBedCountAsync(long pid, long rid, bool checkin);
        //Task<AvailableBedsDTO> GetAvailableBedsPerRoomAsync(Guid userid, Guid propertyid, Guid roomid);
        #endregion

        #region Room Features
        //Task<IResponseModel> AddRoomFeatureAsync(AddRoomFeaturesModel model);
        ////Task<List<RoomFeaturesDTO>> GetRoomFeaturesAsync(Guid propertyid);
        //Task<IResponseModel> VerifyRoomFeatureAsync(long rid, string name);
        //Task<IEnumerable<string>> GetRoomFeaturesAsync(Guid rid);
        #endregion
    }

    public class RoomService : IRoomService
    {
        private IRoomRepository _repo;
        private IResponseService<Response> _responseSvc;
        private IResponseService<IEnumerable<RoomTypesDTO>> _roomTypesResp;

        public RoomService(
            IRoomRepository repo,
            IResponseService<Response> responseSvc,
            IResponseService<IEnumerable<RoomTypesDTO>> roomTypesResp)
        {
            _repo = repo;
            _responseSvc = responseSvc;
            _roomTypesResp = roomTypesResp;
        }

        #region Room Types
        public async Task<IResponseModel<Response>> AddRoomTypesAsync(AddRoomTypeModel model)
        {
            RoomTypes roomType = new RoomTypes
            {
                Type = model.Type,
                Price = model.Price,
                MonthsAdvance = model.MonthsAdvance,
                MonthsDeposit = model.MonthsDeposit,
                DateCreated = DateTime.Now,
                PropertyId = model.PropertyId
            };

            var rmType = await _repo.AddRoomTypesAsync(roomType);

            return rmType != null
                ? _responseSvc.Response(true, "Room type added successfully.", HttpStatusCode.OK)
                : _responseSvc.Response(false, "Error adding room type", HttpStatusCode.BadRequest);
        }

        public async Task<IResponseModel<Response>> VerifyRoomTypeAsync(long pid, string roomType)
        {
            RoomTypesDTO rmType = await _repo.GetRoomTypeAsync(pid, roomType);

            if (rmType != null) return _responseSvc.Response(false, "Room Type already exists.", HttpStatusCode.BadRequest);
            else return _responseSvc.Response(true, "Room Type available.", HttpStatusCode.OK);
        }

        //public async Task<IResponseModel> VerifyRoomTypeAsync(long pid, long roomTypeId)
        //{
        //    RoomTypesDTO rmType = await _repo.GetRoomTypeAsync(pid, roomTypeId);

        //    if (rmType != null) return _responseSvc.Response(true, "Room Type exists.", HttpStatusCode.OK);
        //    else return _responseSvc.Response(false, "Invalid room type.", HttpStatusCode.BadRequest);
        //}

        public async Task<IResponseModel<IEnumerable<RoomTypesDTO>>> GetRoomTypesPerPropertyAsync(long pid)
            => _roomTypesResp.Response(true, "", HttpStatusCode.OK, await _repo.GetRoomTypesPerPropertyAsync(pid));

        //public async Task<IResponseModel> UpdateRoomTypeAsync(UpdateRoomTypeModel obj)
        //{
        //    RoomTypes roomType = new RoomTypes
        //    {
        //        Id = obj.RoomTypeId,
        //        Type = obj.Type,
        //        Price = obj.Price,
        //        MonthsAdvance = obj.MonthsAdvance,
        //        MonthsDeposit = obj.MonthsDeposit,
        //        PropertyId = obj.PropertyId
        //    };

        //    RoomTypes rmType = await _repo.UpdateRoomTypeAsync(roomType);

        //    return rmType != null
        //        ? _responseSvc.Response(true, "Room type successfully updated.", HttpStatusCode.OK)
        //        : _responseSvc.Response(false, "Error updating room type.", HttpStatusCode.BadRequest);
        //}
        #endregion

        #region Room
        //public async Task<IResponseModel> AddRoomAsync(AddRoomModel model)
        //{
        //    Core.Models.Room room = new Core.Models.Room
        //    {
        //        Name = model.Name,
        //        TotalBeds = model.TotalBeds,
        //        OccupiedBeds = 0,
        //        RoomTypeId = model.RoomTypeId,
        //        PropertyId = model.PropertyId,
        //        DateCreated = DateTime.Now
        //    };

        //    var rm = await _repo.AddRoomAsync(room);

        //    return rm != null
        //        ? _responseSvc.Response(true, "Room added successfully.", HttpStatusCode.OK)
        //        : _responseSvc.Response(false, "Error adding room", HttpStatusCode.BadRequest);
        //}

        //public async Task<IResponseModel> VerifyRoomAsync(long pid, string roomname)
        //{
        //    var room = await _repo.GetRoomInfoByNameAsync(pid, roomname);

        //    if (room != null) return _responseSvc.Response(true, "Room already exists.", HttpStatusCode.BadRequest);
        //    else return _responseSvc.Response(false, "Room available.", HttpStatusCode.OK);
        //}

        //public async Task<IEnumerable<RoomsListInfoDTO>> GetPropertyRoomsAsync(long pid, bool filter = false)
        //{
        //    IEnumerable<RoomsListInfoDTO> rooms;

        //    if (filter == true)
        //        rooms = await _repo.GetPropertyRoomsAsync(pid, filter);
        //    else
        //        rooms = await _repo.GetPropertyRoomsAsync(pid);

        //    return rooms.Count() > 0 ? rooms : null;
        //}

        //public async Task<RoomInfoDTO> GetRoomInfoAsync(long pid, long rid)
        //{
        //    var roomInfo = await _repo.GetRoomInfoAsync(pid, rid);

        //    if (roomInfo != null)
        //    {
        //        roomInfo.Features = await _repo.GetRoomFeaturesAsync(rid);
        //        return roomInfo;
        //    }

        //    return null;
        //}

        //public async Task<IResponseModel> UpdateBedCountAsync(long pid, long rid, bool checkin)
        //{
        //    RoomInfoDTO roomInfo = await _repo.GetRoomInfoAsync(pid, rid);
        //    bool isBedCntUpd;

        //    if (roomInfo != null && checkin)
        //    {
        //        int count = roomInfo.OccupiedBeds += 1;
        //        isBedCntUpd = await _repo.UpdateBedCountAsync(pid, rid, count);
        //        return _responseSvc.Response(isBedCntUpd, "Bed count updated.", HttpStatusCode.OK);
        //    }

        //    if (roomInfo != null && !checkin)
        //    {
        //        int count = roomInfo.OccupiedBeds -= 1;
        //        isBedCntUpd = await _repo.UpdateBedCountAsync(pid, rid, count);
        //        return _responseSvc.Response(isBedCntUpd, "Bed count updated.", HttpStatusCode.OK);
        //    }

        //    return _responseSvc.Response(false, "Room not found.", HttpStatusCode.BadRequest);
        //}
        #endregion

        #region Room Features
        //public async Task<IResponseModel> AddRoomFeatureAsync(AddRoomFeaturesModel model)
        //{
        //    RoomFeatures room = new RoomFeatures
        //    {
        //        Name = model.Name,
        //        RoomId = model.RoomId,
        //        DateCreated = DateTime.Now
        //    };

        //    RoomFeatures feature = await _repo.AddRoomFeatureAsync(room);

        //    return feature != null
        //        ? _responseSvc.Response(true, "Room feature added successfully", HttpStatusCode.OK)
        //        : _responseSvc.Response(false, "Error adding room feature.", HttpStatusCode.BadRequest);
        //}

        //public async Task<IResponseModel> VerifyRoomFeatureAsync(long rid, string name)
        //{
        //    string feature = await _repo.GetRoomFeatureAsync(rid, name);

        //    if (!string.IsNullOrEmpty(feature)) return _responseSvc.Response(true, "Feature already exists.", HttpStatusCode.BadRequest);
        //    else return _responseSvc.Response(false, "Feature available.", HttpStatusCode.OK);
        //}

        //public async Task<IEnumerable<string>> GetRoomFeaturesAsync(Guid rid)
        //{
        //    var features = await _repo.GetRoomFeaturesAsync(rid);

        //    return features.Count() > 0 ? features : null;
        //}
        #endregion
    }
}
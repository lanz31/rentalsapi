﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentalsCoreApi31.Infrastructure.Migrations
{
    public partial class Addedstatuspropertytorenterentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "Renters",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "ac4b3fae-42bf-4403-81c9-ba1a68650249");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Renters");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "3b1463b2-18c5-405a-99be-d5f6dc3758e7");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentalsCoreApi31.Infrastructure.Migrations
{
    public partial class AddedRoomTypesFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "MonthsAdvance",
                table: "RoomTypes",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MonthsDeposit",
                table: "RoomTypes",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MonthsAdvance",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "MonthsDeposit",
                table: "RoomTypes");
        }
    }
}

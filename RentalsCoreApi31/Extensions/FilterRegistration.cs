﻿using RentalsCoreApi31.Filters;

using Microsoft.Extensions.DependencyInjection;

namespace RentalsCoreApi31.Registration
{
    public static class FilterRegistration
    {
        public static void RegisterFilters(this IServiceCollection services)
        {
            services.AddScoped<UserIdFilter>();
            services.AddScoped<UserPropIdFilter>();
        }
    }
}

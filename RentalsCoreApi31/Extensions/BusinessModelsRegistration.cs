﻿using RentalsCoreApi31.Core.BusinessModels;

using Microsoft.Extensions.DependencyInjection;

namespace RentalsCoreApi31.Extensions
{
    public static class BusinessModelsRegistration
    {
        public static void RegisterBusinessModels(this IServiceCollection services)
        {
            services.AddScoped(typeof(IResponseModel<>), typeof(ResponseModel<>));
        }
    }
}

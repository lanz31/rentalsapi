﻿using RentalsCoreApi31.Infrastructure.Operations.User.Handlers;
using RentalsCoreApi31.Infrastructure.Operations.User.Validator;
using RentalsCoreApi31.Infrastructure.Operations.Property.Validator;
using RentalsCoreApi31.Infrastructure.Operations.Room.Validator;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Validator;

using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace RentalsCoreApi31.Registration
{
    public static class MediatRRegistration
    {
        public static void RegisterHandlers(this IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<CreateUserCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<LoginUserCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddPropertyCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<GetPropertyInfoQueryValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<GetPropertiesQueryValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<UpdatePropertyInfoCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddRoomTypeCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<GetRoomTypesQueryValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddRenterCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<GetRenterInfoQueryValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<GetRentersQueryValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<CheckinRenterCommandValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<CheckoutRenterCommandValidator>();
            });

            // Get the assembly containing the handlers and register it to mediatr
            services.AddMediatR(typeof(CreateUserCommandHandler).Assembly);
        }
    }
}

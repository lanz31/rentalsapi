﻿using RentalsCoreApi31.Core.Services;
using RentalsCoreApi31.Infrastructure.Operations.User.Repository;
using RentalsCoreApi31.Infrastructure.Operations.User.Services;
using RentalsCoreApi31.Infrastructure.Operations.Property.Repository;
using RentalsCoreApi31.Infrastructure.Operations.Property.Services;
using RentalsCoreApi31.Infrastructure.Operations.Room.Repository;
using RentalsCoreApi31.Infrastructure.Operations.Room.Services;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Repository;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Services;
using RentalsCoreApi31.Infrastructure.Operations.Transactions.Repository;
using RentalsCoreApi31.Infrastructure.Operations.Transactions.Services;
using RentalsCoreApi31.Infrastructure.Services;

using Microsoft.Extensions.DependencyInjection;

namespace RentalsCoreApi31.Registration
{
    public static class ServiceRegistration
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<ILoggerService, LoggerService>();
            services.AddTransient(typeof(IResponseService<>), typeof(ResponseService<>));

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IAccountService, AccountService>();

            services.AddTransient<IPropertyRepository, PropertyRepository>();
            services.AddTransient<IPropertyService, PropertyService>();

            services.AddTransient<IRoomRepository, RoomRepository>();
            services.AddTransient<IRoomService, RoomService>();

            services.AddTransient<IRenterRepository, RenterRepository>();
            services.AddTransient<IRenterService, RenterService>();

            services.AddTransient<ITransactionsRepository, TransactionsRepository>();
            services.AddTransient<ITransactionService, TransactionService>();
        }
    }
}

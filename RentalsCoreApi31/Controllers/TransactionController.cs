﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using System.Threading.Tasks;
using MediatR;

namespace RealEstateCore.Controllers
{
    [Authorize]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/v1/Transaction")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        //private IMediator _mediator;

        //public TransactionController(IMediator mediator)
        //{
        //    _mediator = mediator;
        //}

        //[HttpPost]
        //[Route("checkin")]
        //public async Task<IResponseModel> PostCheckInTransactionAsync([FromBody] CheckinRenterCommand command) => await _mediator.Send(command);
    }
}
﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Property.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Property.Queries;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RentalsCoreApi31.Controllers
{
    [Authorize(Roles = "Admin")]
    [EnableCors("CorsPolicy")]
    [Route("api/v1/Property")]
    [ApiController]
    public class PropertyController : ControllerBase
    {
        private IMediator _mediator;

        public PropertyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("add")]
        public async Task<IResponseModel<Response>> PostAddPropertyAsync([FromBody] AddPropertyCommand command) => await _mediator.Send(command);

        [HttpGet]
        [Route("{pid:long}/info")]
        //[ServiceFilter(typeof(UserPropIdFilter))]
        public async Task<IResponseModel<PropertyInfoDTO>> GetPropertyInfoAsync(long pid)
        {
            var query = new GetPropertyInfoQuery(pid);

            return await _mediator.Send(query);
        }

        [HttpGet]
        [Route("{uid:long}")]
        public async Task<IResponseModel<IEnumerable<PropertyInfoDTO>>> GetPropertiesAsync(long uid)
        {
            var query = new GetPropertiesQuery(uid);

            return await _mediator.Send(query);
        }

        //[HttpPut]
        //[Route("")]
        //public async Task<IResponseModel> PutPropertyInfoAsync([FromBody] UpdatePropertyInfoCommand command) => await _mediator.Send(command);
    }
}
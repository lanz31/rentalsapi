﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.User.Commands;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using MediatR;
using System.Threading.Tasks;

namespace RentalsCoreApi31.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/v1/Account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        //[AllowAnonymous]
        //[HttpPost]
        //[Route("register")]
        //public async Task<IResponseModel> PostRegisterAsync([FromBody] CreateUserCommand command) => await _mediator.Send(command);

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<TokenModel> PostLoginAsync([FromBody] LoginUserCommand command) => await _mediator.Send(command);
    }
}
﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;
using RentalsCoreApi31.Infrastructure.Operations.Room.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Room.Queries;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RentalsCoreApi31.Controllers
{
    [Authorize(Roles = "Admin")]
    [EnableCors("CorsPolicy")]
    [Route("api/v1/Room")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private IMediator _mediator;

        public RoomController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #region Room Type Endpoints
        [HttpPost]
        [Route("type")]
        public async Task<IResponseModel<Response>> PostRoomTypeAsync([FromBody] AddRoomTypeCommand command) => await _mediator.Send(command);

        [HttpGet]
        [Route("{pid:long}/types")]
        public async Task<IResponseModel<IEnumerable<RoomTypesDTO>>> GetRoomTypesAsync(long pid)
        {
            var query = new GetRoomTypesQuery(pid);

            return await _mediator.Send(query);
        }

        //[HttpPut]
        //[Route("type")]
        //public async Task<IResponseModel> PutRoomTypeAsync([FromBody] UpdateRoomTypeCommand command) => await _mediator.Send(command);
        #endregion

        #region Room Endpoints
        //[HttpPost]
        //[Route("")]
        //public async Task<IResponseModel> PostAddRoomAsync([FromBody] AddRoomCommand command) => await _mediator.Send(command);

        //[HttpGet]
        //[Route("{pid:long}/list")]
        //public async Task<IEnumerable<RoomsListInfoDTO>> GetRoomsPerPropertyAsync(long pid, [FromQuery] bool filter = false)
        //{
        //    var query = new GetRoomsQuery(pid, filter);

        //    return await _mediator.Send(query);
        //}

        //[HttpGet]
        //[Route("{pid:long}/info/{rid:long}")]
        //public async Task<RoomInfoDTO> GetRoomInfoAsync(long pid, long rid)
        //{
        //    var query = new GetRoomInfoQuery(pid, rid);

        //    return await _mediator.Send(query);
        //}

        //[HttpGet]
        //[Route("prices")]
        //public async Task<IActionResult> GetRoomsWithPricesAsync([FromQuery] string userid, [FromQuery] string propertyid)
        //{
        //    if (string.IsNullOrEmpty(userid) || string.IsNullOrEmpty(propertyid)) return BadRequest(new ResponseModel { Status = false, Message = "User Id and Property Id is required." });

        //    var userId = Guid.Parse(userid);
        //    var propertyId = Guid.Parse(propertyid);

        //    var response = await _roomService.GetRoomsWithPricesAsync(userId, propertyId);

        //    if (response.Count == 0)
        //        return NoContent();
        //    else
        //        return Ok(response);
        //}

        //[HttpGet]
        //[Route("info")]
        //public async Task<IActionResult> GetRoomInfoAsync([FromQuery] string userid, [FromQuery] string propertyid, [FromQuery] string roomid)
        //{
        //    if (string.IsNullOrEmpty(userid) || string.IsNullOrEmpty(propertyid) || string.IsNullOrEmpty(roomid))
        //        return BadRequest(new ResponseModel { Status = false, Message = "User Id, Property Id and Room Id is required." });

        //    var userId = Guid.Parse(userid);
        //    var propertyId = Guid.Parse(propertyid);
        //    var roomId = Guid.Parse(roomid);

        //    var roomInfo = await _roomService.GetRoomInfoAsync(userId, propertyId, roomId);

        //    if (roomInfo != null)
        //        return Ok(roomInfo);
        //    else
        //        return BadRequest(new ResponseModel { Status = false, Message = "Room info not added yet." });
        //}

        //[HttpGet]
        //[Route("available")]
        //public async Task<IActionResult> GetAvailableBedsPerRoomAsync([FromQuery] Guid userid, [FromQuery] Guid propertyid, [FromQuery] Guid roomid)
        //{
        //    if (string.IsNullOrEmpty(userid.ToString()) || string.IsNullOrEmpty(propertyid.ToString()) || string.IsNullOrEmpty(roomid.ToString()))
        //        return BadRequest(new ResponseModel { Status = false, Message = "User Id, Property Id and Room Id is required." });

        //    var args = new AvailableBedsModel
        //    {
        //        UserId = Guid.Parse(userid.ToString()),
        //        PropertyId = Guid.Parse(propertyid.ToString()),
        //        RoomId = Guid.Parse(roomid.ToString())
        //    };

        //    var availableBeds = await _roomService.GetAvailableBedsPerRoomAsync(userid, propertyid, roomid);

        //    if (availableBeds != null)
        //        return Ok(availableBeds);
        //    else
        //        return BadRequest(new ResponseModel { Status = false, Message = "No available beds." });
        //}
        #endregion

        #region Room Features Endpoints
        //[HttpPost]
        //[Route("features/add")]
        //public async Task<IResponseModel> PostRoomFeatureAsync([FromBody] AddRoomFeatureCommand command) => await _mediator.Send(command);
        #endregion
    }
}
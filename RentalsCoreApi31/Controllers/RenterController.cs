﻿using RentalsCoreApi31.Infrastructure.Operations.Renter.Commands;
using RentalsCoreApi31.Infrastructure.Operations.Renter.Queries;
using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Core.BusinessModels.DTO;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RealEstateCore.Controllers
{
    [Authorize(Roles = "Admin")]
    [EnableCors("CorsPolicy")]
    [Route("api/v1/Renter")]
    [ApiController]
    public class RenterController : ControllerBase
    {
        private IMediator _mediator;

        public RenterController(IMediator mediator)
        {
            _mediator = mediator;
        }

        //[HttpPost]
        //[Route("add")]
        //public async Task<IResponseModel> PostAddRenterAsync([FromBody] AddRenterCommand command) => await _mediator.Send(command);

        //[HttpGet]
        //[Route("{pid:long}/info/{rid:long}")]
        //public async Task<RenterInfoDTO> GetRenterInfoAsync(long pid, long rid)
        //{
        //    var query = new GetRenterInfoQuery(pid, rid);

        //    return await _mediator.Send(query);
        //}

        //[HttpGet]
        //[Route("{pid:long}/list")]
        //public async Task<IEnumerable<RenterListDTO>> GetRentersAsync(long pid)
        //{
        //    var query = new GetRentersQuery(pid);

        //    return await _mediator.Send(query);
        //}

        //[HttpPost]
        //[Route("checkin")]
        //public async Task<IResponseModel> PostCheckInRenterAsync([FromBody] CheckinRenterCommand command) => await _mediator.Send(command);

        //[HttpPut]
        //[Route("checkout")]
        //public async Task<IResponseModel> PutCheckOutRenterAsync([FromBody] CheckoutRenterCommand command) => await _mediator.Send(command);
    }
}
﻿using RentalsCoreApi31.Core.BusinessModels;
using RentalsCoreApi31.Infrastructure.Operations.User.Commands;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using MediatR;

namespace RentalsCoreApi31.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/v1/Token")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private IMediator _mediator;

        public TokenController(IMediator mediator)
        {
            _mediator = mediator;
        }

        //[AllowAnonymous]
        //[HttpPost]
        //[Route("")]
        //public async Task<TokenModel> Authenticate([FromBody] LoginUserCommand command)
        //{
        //    return await _mediator.Send(command);
        //}
    }
}